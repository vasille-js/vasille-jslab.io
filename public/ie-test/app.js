/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/vasille-js/src/attribute.js":
/*!**************************************************!*\
  !*** ./node_modules/vasille-js/src/attribute.js ***!
  \**************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "attributify": function() { return /* binding */ attributify; },
/* harmony export */   "AttributeBinding": function() { return /* binding */ AttributeBinding; }
/* harmony export */ });
/* harmony import */ var _bind_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bind.js */ "./node_modules/vasille-js/src/bind.js");
/* harmony import */ var _interfaces_idefinition_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interfaces/idefinition.js */ "./node_modules/vasille-js/src/interfaces/idefinition.js");
/* harmony import */ var _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./interfaces/ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
/* harmony import */ var _property_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./property.js */ "./node_modules/vasille-js/src/property.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }





/**
 * Creates a attribute 1 to 1 bind
 * @param rt {BaseNode} is the root component
 * @param ts {BaseNode} is the this component
 * @param name {String} is attribute name
 * @param value {?any} is attribute value
 * @param func {?Callable} is attribute value calculation function
 * @returns {AttributeBinding} 1 to 1 bind of attribute
 */

function attributify(rt, ts, name) {
  var value = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  var func = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
  return new AttributeBinding(rt, ts, name, null, (0,_property_js__WEBPACK_IMPORTED_MODULE_3__.propertify)(value, func));
}
/**
 * Represents a Attribute binding description
 * @extends Binding
 */

var AttributeBinding = /*#__PURE__*/function (_Binding) {
  _inherits(AttributeBinding, _Binding);

  var _super = _createSuper(AttributeBinding);

  /**
   * Constructs a attribute binding description
   * @param rt {BaseNode} is root component
   * @param ts {BaseNode} is this component
   * @param name {String} is the name of attribute
   * @param func {?Function} is the function to bound
   * @param values {Array<IValue>} is the array of values to bind to
   */
  function AttributeBinding(rt, ts, name, func) {
    _classCallCheck(this, AttributeBinding);

    for (var _len = arguments.length, values = new Array(_len > 4 ? _len - 4 : 0), _key = 4; _key < _len; _key++) {
      values[_key - 4] = arguments[_key];
    }

    return _super.call.apply(_super, [this, rt, ts, name, func].concat(values));
  }
  /**
   * Generates a function which updates the attribute value
   * @param name {String} The name of attribute
   * @returns {Function} a function which will update attribute value
   */


  _createClass(AttributeBinding, [{
    key: "bound",
    value: function bound(name) {
      return function (rt, ts, value) {
        if (value) {
          rt.$app.$run.setAttribute(ts.el, name, value);
        } else {
          rt.$app.$run.removeAttribute(ts.el, name);
        }

        return value;
      };
    }
  }]);

  return AttributeBinding;
}(_bind_js__WEBPACK_IMPORTED_MODULE_0__.Binding);

/***/ }),

/***/ "./node_modules/vasille-js/src/bind.js":
/*!*********************************************!*\
  !*** ./node_modules/vasille-js/src/bind.js ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Expression": function() { return /* binding */ Expression; },
/* harmony export */   "Binding": function() { return /* binding */ Binding; }
/* harmony export */ });
/* harmony import */ var _interfaces_errors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./interfaces/errors */ "./node_modules/vasille-js/src/interfaces/errors.js");
/* harmony import */ var _interfaces_ibind_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interfaces/ibind.js */ "./node_modules/vasille-js/src/interfaces/ibind.js");
/* harmony import */ var _interfaces_idefinition__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./interfaces/idefinition */ "./node_modules/vasille-js/src/interfaces/idefinition.js");
/* harmony import */ var _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./interfaces/ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
/* harmony import */ var _value_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./value.js */ "./node_modules/vasille-js/src/value.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }






/**
 * Bind some values to one expression
 * @implements IBind
 */

var Expression = /*#__PURE__*/function (_IBind) {
  _inherits(Expression, _IBind);

  var _super = _createSuper(Expression);

  /**
   * The array of value which will trigger recalculation
   * @type {Array<IValue<*>>}
   */

  /**
   * Cache the values of expression variables
   * @type {Array<*>}
   * @version 1.1
   */

  /**
   * The function which will be executed on recalculation
   * @type {Function}
   */

  /**
   * Expression will link different handler for each value of list
   * @type {Array<Function>}
   * @version 1.1
   */

  /**
   * The current linking state
   * @type {boolean}
   */

  /**
   * The buffer to keep the last calculated value
   * @type {Reference<*>}
   */

  /**
   * Creates a function bounded to N value
   * @param func {Function} The function to bound
   * @param values {Array<IValue<*>>} Values to bound to
   * @param link {Boolean} If true links immediately
   */
  function Expression(func, values) {
    var _this;

    var link = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

    _classCallCheck(this, Expression);

    _this = _super.call(this);
    _this.values = void 0;
    _this.valuesCache = [];
    _this.func = void 0;
    _this.linkedFunc = [];
    _this.linked = void 0;
    _this.sync = new _value_js__WEBPACK_IMPORTED_MODULE_4__.Reference(null);

    var handler = function handler(i) {
      if (i != null) {
        _this.valuesCache[i] = _this.values[i].$;
      }

      var value = func.apply(_assertThisInitialized(_this), _this.valuesCache);

      if (_this.type) {
        if (!(0,_interfaces_idefinition__WEBPACK_IMPORTED_MODULE_2__.checkType)(value, _this.type)) {
          throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.typeError)("expression returns wrong incompatible value");
        }
      }

      _this.sync.$ = value;
    };

    var i = 0;

    var _iterator = _createForOfIteratorHelper(values),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var value = _step.value;

        _this.valuesCache.push(value.$);

        _this.linkedFunc.push(handler.bind(_assertThisInitialized(_this), Number(i++)));
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }

    _this.values = values;
    _this.func = handler;
    _this.linked = false;

    if (link) {
      _this.link();
    } else {
      handler(null);
    }

    return _this;
  }
  /**
   * Gets the last calculated value
   * @return {*} The last calculated value
   */


  _createClass(Expression, [{
    key: "$",
    get: function get() {
      return this.sync.$;
    }
    /**
     * Sets the last calculated value in manual mode
     * @param value {*} New value for last calculated value
     * @return {Expression<*>} A pointer to this
     */
    ,
    set: function set(value) {
      this.sync.$ = value;
      return this;
    }
    /**
     * Sets a user handler on value change
     * @param handler {Function} User defined handler
     * @return {this} A pointer to this
     */

  }, {
    key: "on",
    value: function on(handler) {
      this.sync.on(handler);
      return this;
    }
    /**
     * Unsets a user handler from value change
     * @param handler {Function} User installed handler
     * @return {this} A pointer to this
     */

  }, {
    key: "off",
    value: function off(handler) {
      this.sync.off(handler);
      return this;
    }
    /**
     * Binds function to each value
     * @returns {this} A pointer to this
     */

  }, {
    key: "link",
    value: function link() {
      if (!this.linked) {
        for (var i = 0; i < this.values.length; i++) {
          this.values[i].on(this.linkedFunc[i]);
          this.valuesCache[i] = this.values[i].$;
        }

        this.func(null);
        this.linked = true;
      }

      return this;
    }
    /**
     * Unbind function from each value
     * @returns {this} A pointer to this
     */

  }, {
    key: "unlink",
    value: function unlink() {
      if (this.linked) {
        for (var i = 0; i < this.values.length; i++) {
          this.values[i].off(this.linkedFunc[i]);
        }

        this.linked = false;
      }

      return this;
    }
    /**
     * Clear bindings on destroy
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      this.unlink();
      this.values.splice(0);
      this.valuesCache.splice(0);
      this.linkedFunc.splice(0);
      this.func = null;
    }
  }]);

  return Expression;
}(_interfaces_ibind_js__WEBPACK_IMPORTED_MODULE_1__.IBind);
/**
 * Describe a common binding logic
 * @implements IValue
 */

var Binding = /*#__PURE__*/function (_IValue) {
  _inherits(Binding, _IValue);

  var _super2 = _createSuper(Binding);

  /**
   * Constructs a common binding logic
   * @param rt {BaseNode} Root component
   * @param ts {BaseNode} This component
   * @param name {String} Name of property/attribute
   * @param func {?Function} A function to run on value change
   * @param values {Array<IValue>} values array to bind
   */
  function Binding(rt, ts, name, func) {
    var _this2;

    _classCallCheck(this, Binding);

    _this2 = _super2.call(this);
    _this2.binding = void 0;
    _this2.func = void 0;
    _this2.owner = void 0;
    _this2.func = _this2.bound(name).bind(null, rt, ts);

    for (var _len = arguments.length, values = new Array(_len > 4 ? _len - 4 : 0), _key = 4; _key < _len; _key++) {
      values[_key - 4] = arguments[_key];
    }

    if (!func && values.length === 1) {
      _this2.binding = values[0];
      _this2.owner = false;
    } else if (func && values.length) {
      _this2.binding = new Expression(func, values);
      _this2.owner = true;
    } else {
      throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.wrongBinding)("Binding request a value as minimum");
    }

    _this2.binding.on(_this2.func);

    _this2.func(_this2.binding.$);

    return _this2;
  }
  /**
   * Is a virtual function to get the specific bind function
   * @param name {String} The name of attribute/property
   * @returns {Function} A function to update attribute/property value
   * @throws Always trows and must be overloaded in child class
   */


  _createClass(Binding, [{
    key: "bound",
    value: function bound(name) {
      throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.notOverwritten)();
    }
  }, {
    key: "$",
    get:
    /**
     * Gets the binding value
     * @return {*} The binding value
     */
    function get() {
      return this.binding.$;
    }
    /**
     * Sets the binding value
     * @param any {*} The new binding value
     * @return {this} A pointer to this
     */
    ,
    set: function set(any) {
      this.binding.$ = any;
      return this;
    }
    /**
     * Adds a user handler to the binding value change event
     * @param func {Function} User defined handler
     * @return {Binding} A pointer to this
     */

  }, {
    key: "on",
    value: function on(func) {
      this.binding.on(func);
      return this;
    }
    /**
     * Removes a user handler from binding value change event
     * @param func {Function} User installed handler
     * @return {Binding} A pointer to this
     */

  }, {
    key: "off",
    value: function off(func) {
      this.binding.off(func);
      return this;
    }
    /**
     * Just clear bindings
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      this.binding.off(this.func);

      if (this.owner) {
        this.binding.$destroy();
      }
    }
  }]);

  return Binding;
}(_interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_3__.IValue);

/***/ }),

/***/ "./node_modules/vasille-js/src/class.js":
/*!**********************************************!*\
  !*** ./node_modules/vasille-js/src/class.js ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "classify": function() { return /* binding */ classify; },
/* harmony export */   "ClassBinding": function() { return /* binding */ ClassBinding; }
/* harmony export */ });
/* harmony import */ var _bind_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bind.js */ "./node_modules/vasille-js/src/bind.js");
/* harmony import */ var _interfaces_idefinition_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interfaces/idefinition.js */ "./node_modules/vasille-js/src/interfaces/idefinition.js");
/* harmony import */ var _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./interfaces/ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
/* harmony import */ var _property_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./property.js */ "./node_modules/vasille-js/src/property.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }





/**
 * Creates a class target 1 to 1 bind
 * @param rt {BaseNode} is the root component
 * @param ts {BaseNode} is the this component
 * @param name {String} is attribute name
 * @param value {?any} is attribute value
 * @param func {?Callable} is attribute value calculation function
 * @returns {AttributeBinding} 1 to 1 bind of attribute
 */

function classify(rt, ts, name) {
  var value = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  var func = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
  return new ClassBinding(rt, ts, name, null, (0,_property_js__WEBPACK_IMPORTED_MODULE_3__.propertify)(value, func));
}
/**
 * Represents a HTML class binding description
 * @extends Binding
 */

var ClassBinding = /*#__PURE__*/function (_Binding) {
  _inherits(ClassBinding, _Binding);

  var _super = _createSuper(ClassBinding);

  /**
   * Constructs a HTML class binding description
   * @param rt {BaseNode} is root component
   * @param ts {BaseNode} is this component
   * @param name {String} is the name of attribute
   * @param func {?Function} is the function to bound
   * @param values {Array<IValue>} is the array of values to bind to
   */
  function ClassBinding(rt, ts, name, func) {
    var _this;

    _classCallCheck(this, ClassBinding);

    for (var _len = arguments.length, values = new Array(_len > 4 ? _len - 4 : 0), _key = 4; _key < _len; _key++) {
      values[_key - 4] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this, rt, ts, name, func].concat(values));
    _this.current = null;
    return _this;
  }
  /**
   * Generates a function which updates the html class value
   * @param name {String} The name of attribute
   * @returns {Function} a function which will update attribute value
   */


  _createClass(ClassBinding, [{
    key: "bound",
    value: function bound(name) {
      var _this2 = this;

      function addClass(rt, ts, cl) {
        rt.$.app.$run.addClass(ts.$.el, cl);
      }

      function removeClass(rt, ts, cl) {
        rt.$.app.$run.removeClass(ts.$.el, cl);
      }

      return function (rt, ts, value) {
        var current = _this2.current;

        if (value !== current) {
          if (typeof current === "string" && current !== "") {
            removeClass(rt, ts, current);
          }

          if (typeof value === "boolean") {
            if (value) {
              addClass(rt, ts, name);
            } else {
              removeClass(rt, ts, name);
            }
          } else if (typeof value === "string" && value !== "") {
            addClass(rt, ts, value);
          }

          _this2.current = value;
        }

        return value;
      };
    }
  }]);

  return ClassBinding;
}(_bind_js__WEBPACK_IMPORTED_MODULE_0__.Binding);

/***/ }),

/***/ "./node_modules/vasille-js/src/executor.js":
/*!*************************************************!*\
  !*** ./node_modules/vasille-js/src/executor.js ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Executor": function() { return /* binding */ Executor; },
/* harmony export */   "InstantExecutor": function() { return /* binding */ InstantExecutor; }
/* harmony export */ });
/* harmony import */ var _interfaces_errors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./interfaces/errors */ "./node_modules/vasille-js/src/interfaces/errors.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }


/**
 * Represents an executor unit interface
 */

var Executor = /*#__PURE__*/function () {
  function Executor() {
    _classCallCheck(this, Executor);
  }

  _createClass(Executor, [{
    key: "addClass",
    value:
    /**
     * Adds a CSS class
     * @param el {HTMLElement} HTML element
     * @param cl {string}
     */
    function addClass(el, cl) {
      throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.notOverwritten)();
    }
    /**
     * Removes a CSS class
     * @param el {HTMLElement} HTML element
     * @param cl {string}
     */

  }, {
    key: "removeClass",
    value: function removeClass(el, cl) {
      throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.notOverwritten)();
    }
    /**
     * Sets a tag attribute
     * @param el {HTMLElement} HTML element
     * @param name {string}
     * @param value {string}
     */

  }, {
    key: "setAttribute",
    value: function setAttribute(el, name, value) {
      throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.notOverwritten)();
    }
    /**
     * Removes a tag attribute
     * @param el {HTMLElement} HTML element
     * @param name {string}
     */

  }, {
    key: "removeAttribute",
    value: function removeAttribute(el, name) {
      throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.notOverwritten)();
    }
    /**
     * Sets a style attribute
     * @param el {HTMLElement} HTML element
     * @param prop {string}
     * @param value {string}
     */

  }, {
    key: "setStyle",
    value: function setStyle(el, prop, value) {
      throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.notOverwritten)();
    }
    /**
     * Inserts a child
     * @param el {HTMLElement} HTML element
     * @param child {HTMLElement | Text | Comment} Child to insert
     * @param before {HTMLElement | Text | Comment} Child used as position locator
     */

  }, {
    key: "insertBefore",
    value: function insertBefore(el, child, before) {
      throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.notOverwritten)();
    }
    /**
     * Appends a child
     * @param el {HTMLElement} HTML element
     * @param child {HTMLElement | Text | Comment} Child to append
     */

  }, {
    key: "appendChild",
    value: function appendChild(el, child) {
      throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.notOverwritten)();
    }
    /**
     * Calls a call-back function
     * @param cb {Function} call-back function
     */

  }, {
    key: "callCallback",
    value: function callCallback(cb) {
      throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.notOverwritten)();
    }
  }]);

  return Executor;
}();
var InstantExecutor = /*#__PURE__*/function (_Executor) {
  _inherits(InstantExecutor, _Executor);

  var _super = _createSuper(InstantExecutor);

  function InstantExecutor() {
    _classCallCheck(this, InstantExecutor);

    return _super.apply(this, arguments);
  }

  _createClass(InstantExecutor, [{
    key: "addClass",
    value:
    /**
     * Adds a CSS class
     * @param el {HTMLElement} HTML element
     * @param cl {string}
     */
    function addClass(el, cl) {
      if (el) {
        el.classList.add(cl);
      }
    }
    /**
     * Removes a CSS class
     * @param el {HTMLElement} HTML element
     * @param cl {string}
     */

  }, {
    key: "removeClass",
    value: function removeClass(el, cl) {
      if (el) {
        el.classList.remove(cl);
      }
    }
    /**
     * Sets a tag attribute
     * @param el {HTMLElement} HTML element
     * @param name {string}
     * @param value {string}
     */

  }, {
    key: "setAttribute",
    value: function setAttribute(el, name, value) {
      el.setAttribute(name, value);
    }
    /**
     * Removes a tag attribute
     * @param el {HTMLElement} HTML element
     * @param name {string}
     */

  }, {
    key: "removeAttribute",
    value: function removeAttribute(el, name) {
      el.removeAttribute(name);
    }
    /**
     * Sets a style attribute
     * @param el {HTMLElement} HTML element
     * @param prop {string}
     * @param value {string}
     */

  }, {
    key: "setStyle",
    value: function setStyle(el, prop, value) {
      el.style.setProperty(prop, value);
    }
    /**
     * Inserts a child
     * @param el {HTMLElement} HTML element
     * @param child {HTMLElement | Text | Comment} Child to insert
     * @param before {HTMLElement | Text | Comment} Child used as position locator
     */

  }, {
    key: "insertBefore",
    value: function insertBefore(el, child, before) {
      el.insertBefore(child, before);
    }
    /**
     * Appends a child
     * @param el {HTMLElement} HTML element
     * @param child {HTMLElement | Text | Comment} Child to append
     */

  }, {
    key: "appendChild",
    value: function appendChild(el, child) {
      el.appendChild(child);
    }
    /**
     * Calls a call-back function
     * @param cb {Function} call-back function
     */

  }, {
    key: "callCallback",
    value: function callCallback(cb) {
      cb();
    }
  }]);

  return InstantExecutor;
}(Executor);

/***/ }),

/***/ "./node_modules/vasille-js/src/index.js":
/*!**********************************************!*\
  !*** ./node_modules/vasille-js/src/index.js ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Expression": function() { return /* reexport safe */ _bind_js__WEBPACK_IMPORTED_MODULE_0__.Expression; },
/* harmony export */   "Destroyable": function() { return /* reexport safe */ _interfaces_destroyable_js__WEBPACK_IMPORTED_MODULE_1__.Destroyable; },
/* harmony export */   "IBind": function() { return /* reexport safe */ _interfaces_ibind_js__WEBPACK_IMPORTED_MODULE_2__.IBind; },
/* harmony export */   "IValue": function() { return /* reexport safe */ _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_3__.IValue; },
/* harmony export */   "ArrayModel": function() { return /* reexport safe */ _models_js__WEBPACK_IMPORTED_MODULE_4__.ArrayModel; },
/* harmony export */   "MapModel": function() { return /* reexport safe */ _models_js__WEBPACK_IMPORTED_MODULE_4__.MapModel; },
/* harmony export */   "ObjectModel": function() { return /* reexport safe */ _models_js__WEBPACK_IMPORTED_MODULE_4__.ObjectModel; },
/* harmony export */   "SetModel": function() { return /* reexport safe */ _models_js__WEBPACK_IMPORTED_MODULE_4__.SetModel; },
/* harmony export */   "AppNode": function() { return /* reexport safe */ _node_js__WEBPACK_IMPORTED_MODULE_5__.AppNode; },
/* harmony export */   "BaseNode": function() { return /* reexport safe */ _node_js__WEBPACK_IMPORTED_MODULE_5__.BaseNode; },
/* harmony export */   "TagNode": function() { return /* reexport safe */ _node_js__WEBPACK_IMPORTED_MODULE_5__.TagNode; },
/* harmony export */   "ExtensionNode": function() { return /* reexport safe */ _node_js__WEBPACK_IMPORTED_MODULE_5__.ExtensionNode; },
/* harmony export */   "TextNode": function() { return /* reexport safe */ _node_js__WEBPACK_IMPORTED_MODULE_5__.TextNode; },
/* harmony export */   "UserNode": function() { return /* reexport safe */ _node_js__WEBPACK_IMPORTED_MODULE_5__.UserNode; },
/* harmony export */   "Pointer": function() { return /* reexport safe */ _value_js__WEBPACK_IMPORTED_MODULE_6__.Pointer; },
/* harmony export */   "Reference": function() { return /* reexport safe */ _value_js__WEBPACK_IMPORTED_MODULE_6__.Reference; },
/* harmony export */   "ArrayView": function() { return /* reexport safe */ _views_js__WEBPACK_IMPORTED_MODULE_7__.ArrayView; },
/* harmony export */   "MapView": function() { return /* reexport safe */ _views_js__WEBPACK_IMPORTED_MODULE_7__.MapView; },
/* harmony export */   "ObjectView": function() { return /* reexport safe */ _views_js__WEBPACK_IMPORTED_MODULE_7__.ObjectView; },
/* harmony export */   "SetView": function() { return /* reexport safe */ _views_js__WEBPACK_IMPORTED_MODULE_7__.SetView; }
/* harmony export */ });
/* harmony import */ var _bind_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bind.js */ "./node_modules/vasille-js/src/bind.js");
/* harmony import */ var _interfaces_destroyable_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interfaces/destroyable.js */ "./node_modules/vasille-js/src/interfaces/destroyable.js");
/* harmony import */ var _interfaces_ibind_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./interfaces/ibind.js */ "./node_modules/vasille-js/src/interfaces/ibind.js");
/* harmony import */ var _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./interfaces/ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
/* harmony import */ var _models_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./models.js */ "./node_modules/vasille-js/src/models.js");
/* harmony import */ var _node_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node.js */ "./node_modules/vasille-js/src/node.js");
/* harmony import */ var _value_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./value.js */ "./node_modules/vasille-js/src/value.js");
/* harmony import */ var _views_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./views.js */ "./node_modules/vasille-js/src/views.js");










/***/ }),

/***/ "./node_modules/vasille-js/src/interfaces/core.js":
/*!********************************************************!*\
  !*** ./node_modules/vasille-js/src/interfaces/core.js ***!
  \********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "$destroyObject": function() { return /* binding */ $destroyObject; },
/* harmony export */   "VasilleNodePrivate": function() { return /* binding */ VasilleNodePrivate; },
/* harmony export */   "VasilleNode": function() { return /* binding */ VasilleNode; }
/* harmony export */ });
/* harmony import */ var _destroyable_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./destroyable.js */ "./node_modules/vasille-js/src/interfaces/destroyable.js");
/* harmony import */ var _errors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./errors */ "./node_modules/vasille-js/src/interfaces/errors.js");
/* harmony import */ var _ivalue_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }





/**
 * Destroy all destroyable object fields
 * @param obj {Object<any, any>} Object to be iterated
 */
function $destroyObject(obj) {
  for (var i in obj) {
    if (obj.hasOwnProperty(i)) {
      var prop = obj[i];

      if (prop instanceof _destroyable_js__WEBPACK_IMPORTED_MODULE_0__.Destroyable && !(prop instanceof VasilleNode)) {
        prop.$destroy();
      }

      delete obj[i];
    }
  }
}
/**
 * Represents a Vasille.js node
 * @implements Destroyable
 */

var VasilleNodePrivate = /*#__PURE__*/function (_Destroyable) {
  _inherits(VasilleNodePrivate, _Destroyable);

  var _super = _createSuper(VasilleNodePrivate);

  /**
   * The encapsulated element
   * @type {HTMLElement | Text | Comment}
   * @see VasilleNode#coreEl
   * @see VasilleNode#el
   * @see VasilleNode#text
   * @see VasilleNode#comment
   */

  /**
   * The collection of attributes
   * @type {Object<String, IValue>}
   * @see VasilleNode#attr
   */

  /**
   * The collection of style attributes
   * @type {Object<String, IValue>}
   * @see VasilleNode#style
   */

  /**
   * The root node
   * @type {BaseNode}
   */

  /**
   * The this node
   * @type {BaseNode}
   */

  /**
   * The app node
   * @type {VasilleNode}
   */

  /**
   * A link to a parent node
   * @type {VasilleNode}
   */

  /**
   * The next node
   * @type {?VasilleNode}
   */

  /**
   * The previous node
   * @type {?VasilleNode}
   */
  function VasilleNodePrivate() {
    var _this;

    _classCallCheck(this, VasilleNodePrivate);

    _this = _super.call(this);
    _this.$el = void 0;
    _this.$attrs = {};
    _this.$style = {};
    _this.root = void 0;
    _this.ts = void 0;
    _this.app = void 0;
    _this.parent = void 0;
    _this.next = void 0;
    _this.prev = void 0;
    return _this;
  }
  /**
   * Gets the encapsulated element anyway
   * @type {HTMLElement | Text | Comment}
   * @see VasilleNode#$el
   */


  _createClass(VasilleNodePrivate, [{
    key: "coreEl",
    get: function get() {
      return this.$el;
    }
    /**
     * Gets the encapsulated element if it is a html element, otherwise undefined
     * @type {HTMLElement}
     * @see VasilleNode#$el
     */

  }, {
    key: "el",
    get: function get() {
      var el = this.coreEl;

      if (el instanceof HTMLElement) {
        return el;
      }

      throw (0,_errors__WEBPACK_IMPORTED_MODULE_1__.internalError)("wrong VasilleNode.$el() call");
    }
    /**
     * Gets the encapsulated element if it is a html text node, otherwise undefined
     * @type {Text}
     * @see VasilleNode#$el
     */

  }, {
    key: "text",
    get: function get() {
      var el = this.coreEl;

      if (el instanceof Text) {
        return el;
      }

      throw (0,_errors__WEBPACK_IMPORTED_MODULE_1__.internalError)("wrong VasilleNode.$text() call");
    }
    /**
     * Gets the encapsulated element if it is a html comment, otherwise undefined
     * @type {Comment}
     * @see VasilleNode#$el
     */

  }, {
    key: "comment",
    get: function get() {
      var el = this.coreEl;

      if (el instanceof Comment) {
        return el;
      }

      throw (0,_errors__WEBPACK_IMPORTED_MODULE_1__.internalError)("wrong VasilleNode.$comment() call");
    }
    /**
     * Encapsulate element
     * @param el {HTMLElement | Text | Comment} element to encapsulate
     * @private
     */

  }, {
    key: "encapsulate",
    value: function encapsulate(el) {
      this.$el = el;
      return this;
    }
    /**
     * Pre-initializes the base of a node
     * @param app {App} the app node
     * @param rt {BaseNode} The root node
     * @param ts {BaseNode} The this node
     * @param before {?VasilleNode} VasilleNode to paste this after
     */

  }, {
    key: "preinit",
    value: function preinit(app, rt, ts, before) {
      this.app = app;
      this.root = rt;
      this.ts = ts;
    }
    /**
     * Gets the component life attribute value
     * @param field {string} attribute name
     * @return {IValue}
     */

  }, {
    key: "attr",
    value: function attr(field) {
      var v = this.$attrs[field];

      if (v instanceof _ivalue_js__WEBPACK_IMPORTED_MODULE_2__.IValue) {
        return v;
      }

      throw (0,_errors__WEBPACK_IMPORTED_MODULE_1__.notFound)("no such attribute: " + field);
    }
    /**
     * Gets the component life style attribute
     * @param field {Object<String, IValue>}
     * @return {IValue<string>}
     * @see VasilleNode#$style
     */

  }, {
    key: "style",
    value: function style(field) {
      var v = this.$style[field];

      if (v instanceof _ivalue_js__WEBPACK_IMPORTED_MODULE_2__.IValue) {
        return v;
      }

      throw (0,_errors__WEBPACK_IMPORTED_MODULE_1__.notFound)("no such style attribute: " + field);
    }
    /**
     * Unlinks all bindings
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      $destroyObject(this.$attrs);
      $destroyObject(this.$style); //$FlowFixMe

      this.$el = null; //$FlowFixMe

      this.$attrs = null; //$FlowFixMe

      this.$style = null; //$FlowFixMe

      this.root = null; //$FlowFixMe

      this.ts = null; //$FlowFixMe

      this.app = null; //$FlowFixMe

      this.parent = null; //$FlowFixMe

      this.next = null; //$FlowFixMe

      this.prev = null;
    }
  }]);

  return VasilleNodePrivate;
}(_destroyable_js__WEBPACK_IMPORTED_MODULE_0__.Destroyable);
/**
 * This class is symbolic
 */

var VasilleNode = /*#__PURE__*/function (_Destroyable2) {
  _inherits(VasilleNode, _Destroyable2);

  var _super2 = _createSuper(VasilleNode);

  /**
   * @type {VasilleNodePrivate}
   */

  /**
   * Constructs a Vasille Node
   * @param $ {VasilleNodePrivate}
   */
  function VasilleNode($) {
    var _this2;

    _classCallCheck(this, VasilleNode);

    _this2 = _super2.call(this);
    _this2.$ = void 0;
    _this2.$ = $ || new VasilleNodePrivate();
    return _this2;
  }

  return VasilleNode;
}(_destroyable_js__WEBPACK_IMPORTED_MODULE_0__.Destroyable);

/***/ }),

/***/ "./node_modules/vasille-js/src/interfaces/destroyable.js":
/*!***************************************************************!*\
  !*** ./node_modules/vasille-js/src/interfaces/destroyable.js ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Destroyable": function() { return /* binding */ Destroyable; }
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * Mark an object which can be destroyed
 * @interface
 */
var Destroyable = /*#__PURE__*/function () {
  function Destroyable() {
    _classCallCheck(this, Destroyable);
  }

  _createClass(Destroyable, [{
    key: "$destroy",
    value:
    /**
     * Garbage collector method
     */
    function $destroy() {}
  }]);

  return Destroyable;
}();

/***/ }),

/***/ "./node_modules/vasille-js/src/interfaces/errors.js":
/*!**********************************************************!*\
  !*** ./node_modules/vasille-js/src/interfaces/errors.js ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "notOverwritten": function() { return /* binding */ notOverwritten; },
/* harmony export */   "internalError": function() { return /* binding */ internalError; },
/* harmony export */   "userError": function() { return /* binding */ userError; },
/* harmony export */   "typeError": function() { return /* binding */ typeError; },
/* harmony export */   "notFound": function() { return /* binding */ notFound; },
/* harmony export */   "wrongBinding": function() { return /* binding */ wrongBinding; }
/* harmony export */ });
var reportIt = "Report it here: https://gitlab.com/vasille-js/vasille-js/-/issues";
/**
 * @return {string}
 */

function notOverwritten() {
  console.error("Vasille.js: Internal error", "Must be overwritten", reportIt);
  return "not-overwritten";
}
/**
 * @return {string}
 */

function internalError(msg) {
  console.error("Vasille.js: Internal error", msg, reportIt);
  return "internal-error";
}
/**
 * @return {string}
 */

function userError(msg, err) {
  console.error("Vasille.js: User error", msg);
  return err;
}
/**
 * @return {string}
 */

function typeError(msg) {
  return userError(msg, "type-error");
}
/**
 * @return {string}
 */

function notFound(msg) {
  return userError(msg, "not-found");
}
/**
 * @return {string}
 */

function wrongBinding(msg) {
  return userError(msg, "wrong-binding");
}

/***/ }),

/***/ "./node_modules/vasille-js/src/interfaces/ibind.js":
/*!*********************************************************!*\
  !*** ./node_modules/vasille-js/src/interfaces/ibind.js ***!
  \*********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IBind": function() { return /* binding */ IBind; }
/* harmony export */ });
/* harmony import */ var _errors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./errors */ "./node_modules/vasille-js/src/interfaces/errors.js");
/* harmony import */ var _ivalue_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }



/**
 * Mark an object which can be bound
 * @interface
 */

var IBind = /*#__PURE__*/function (_IValue) {
  _inherits(IBind, _IValue);

  var _super = _createSuper(IBind);

  function IBind() {
    _classCallCheck(this, IBind);

    return _super.apply(this, arguments);
  }

  _createClass(IBind, [{
    key: "link",
    value:
    /**
     * Ensure the binding to be bound
     * @return a pointer to this
     * @throws must be overwritten
     */
    function link() {
      throw (0,_errors__WEBPACK_IMPORTED_MODULE_0__.notOverwritten)();
    }
    /**
     * Ensure the binding to be unbound
     * @return a pointer to this
     * @throws must be overwritten
     */

  }, {
    key: "unlink",
    value: function unlink() {
      throw (0,_errors__WEBPACK_IMPORTED_MODULE_0__.notOverwritten)();
    }
  }]);

  return IBind;
}(_ivalue_js__WEBPACK_IMPORTED_MODULE_1__.IValue);

/***/ }),

/***/ "./node_modules/vasille-js/src/interfaces/idefinition.js":
/*!***************************************************************!*\
  !*** ./node_modules/vasille-js/src/interfaces/idefinition.js ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Callable": function() { return /* binding */ Callable; },
/* harmony export */   "checkType": function() { return /* binding */ checkType; },
/* harmony export */   "isSubclassOf": function() { return /* binding */ isSubclassOf; }
/* harmony export */ });
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./node_modules/vasille-js/src/index.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }


/**
 * Represent a function encapsulated to a class
 */

var Callable =
/**
 * Function which will resolve value
 * @type {Function}
 */

/**
 * Encapsulates a function
 * @param func {Function} function to encapsulate
 */
function Callable(func) {
  _classCallCheck(this, Callable);

  this.func = void 0;
  this.func = func;
};
/**
 * Check the type of value
 * @param v {*} value
 * @param t {Function} type constructor
 * @return {boolean}
 */

function checkType(v, t) {
  if (v instanceof _index__WEBPACK_IMPORTED_MODULE_0__.IValue) {
    v = v.$;
  }

  return v instanceof t || v === null || typeof v === "number" && t === Number || typeof v === "string" && t === String || typeof v === "boolean" && t === Boolean;
}
/**
 * Check if a type is a subtype
 * @param parent {Function} potential superclass
 * @param child {Function} potential subclass
 * @return {boolean}
 */

function isSubclassOf(parent, child) {
  var it = child;
  var isParent = false;

  while (it && !isParent) {
    isParent = it.prototype instanceof parent;
    it = it.prototype;
  }

  return isParent;
}

/***/ }),

/***/ "./node_modules/vasille-js/src/interfaces/ivalue.js":
/*!**********************************************************!*\
  !*** ./node_modules/vasille-js/src/interfaces/ivalue.js ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IValue": function() { return /* binding */ IValue; }
/* harmony export */ });
/* harmony import */ var _destroyable_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./destroyable.js */ "./node_modules/vasille-js/src/interfaces/destroyable.js");
/* harmony import */ var _errors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./errors */ "./node_modules/vasille-js/src/interfaces/errors.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }



/**
 * A interface which describes a value
 * @interface
 * @implements Destroyable
 */

var IValue = /*#__PURE__*/function (_Destroyable) {
  _inherits(IValue, _Destroyable);

  var _super = _createSuper(IValue);

  function IValue() {
    var _this;

    _classCallCheck(this, IValue);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.type = null;
    return _this;
  }

  _createClass(IValue, [{
    key: "$",
    get:
    /**
     * Gets the encapsulated value
     * @return {*} Must return a value
     * @throws Must be overwritten
     */
    function get() {
      throw (0,_errors__WEBPACK_IMPORTED_MODULE_1__.notOverwritten)();
    }
    /**
     * Sets the encapsulated value
     * @param value {*} Reference to encapsulate
     * @return {IValue} A pointer to this
     * @throws Must be overwritten
     */
    ,
    set: function set(value) {
      throw (0,_errors__WEBPACK_IMPORTED_MODULE_1__.notOverwritten)();
    }
    /**
     * Add a new handler to value change
     * @param handler {Function} The handler to add
     * @return {IValue} a pointer to this
     * @throws Must be overwritten
     */

  }, {
    key: "on",
    value: function on(handler) {
      throw (0,_errors__WEBPACK_IMPORTED_MODULE_1__.notOverwritten)();
    }
    /**
     * Removes a handler of value change
     * @param handler {Function} the handler to remove
     * @return {IValue} a pointer to this
     * @throws Must be overwritten
     */

  }, {
    key: "off",
    value: function off(handler) {
      throw (0,_errors__WEBPACK_IMPORTED_MODULE_1__.notOverwritten)();
    }
  }]);

  return IValue;
}(_destroyable_js__WEBPACK_IMPORTED_MODULE_0__.Destroyable);

/***/ }),

/***/ "./node_modules/vasille-js/src/models.js":
/*!***********************************************!*\
  !*** ./node_modules/vasille-js/src/models.js ***!
  \***********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ArrayModel": function() { return /* binding */ ArrayModel; },
/* harmony export */   "ObjectModel": function() { return /* binding */ ObjectModel; },
/* harmony export */   "MapModel": function() { return /* binding */ MapModel; },
/* harmony export */   "SetModel": function() { return /* binding */ SetModel; },
/* harmony export */   "vassilify": function() { return /* binding */ vassilify; }
/* harmony export */ });
/* harmony import */ var _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./interfaces/ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
/* harmony import */ var _value_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./value.js */ "./node_modules/vasille-js/src/value.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



/**
 * Represent a listener for a model
 */

var Listener = /*#__PURE__*/function () {
  function Listener() {
    _classCallCheck(this, Listener);

    this.onAdded = new Set();
    this.onRemoved = new Set();
  }

  _createClass(Listener, [{
    key: "emitAdded",
    value:
    /**
     * Emits added event to listeners
     * @param index {number | string | *} index of value
     * @param value {IValue} value of added item
     */
    function emitAdded(index, value) {
      var _iterator = _createForOfIteratorHelper(this.onAdded),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var handler = _step.value;
          handler(index, value);
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
    }
    /**
     * Emits removed event to listeners
     * @param index {number | string | *} index of removed value
     * @param value {IValue} value of removed item
     */

  }, {
    key: "emitRemoved",
    value: function emitRemoved(index, value) {
      var _iterator2 = _createForOfIteratorHelper(this.onRemoved),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var handler = _step2.value;
          handler(index, value);
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
    }
    /**
     * Adds an handler to added event
     * @param handler {Function} function to run on event emitting
     */

  }, {
    key: "onAdd",
    value: function onAdd(handler) {
      this.onAdded.add(handler);
    }
    /**
     * Adds an handler to removed event
     * @param handler {Function} function to run on event emitting
     */

  }, {
    key: "onRemove",
    value: function onRemove(handler) {
      this.onRemoved.add(handler);
    }
    /**
     * Removes an handler from added event
     * @param handler {Function} handler to remove
     */

  }, {
    key: "offAdd",
    value: function offAdd(handler) {
      this.onAdded["delete"](handler);
    }
    /**
     * Removes an handler form removed event
     * @param handler {Function} handler to remove
     */

  }, {
    key: "offRemove",
    value: function offRemove(handler) {
      this.onRemoved["delete"](handler);
    }
  }]);

  return Listener;
}();
/**
 * Model based on Array class
 * @extends Array<IValue>
 */


var ArrayModel = /*#__PURE__*/function (_Array) {
  _inherits(ArrayModel, _Array);

  var _super = _createSuper(ArrayModel);

  /**
   * Listener of array model
   * @type {Listener}
   */

  /* Constructor */

  /**
   * Constructs an array model from an array
   * @param data {Array<IValue>} input data
   */
  function ArrayModel() {
    var _thisSuper, _this;

    var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

    _classCallCheck(this, ArrayModel);

    _this = _super.call(this);
    _this.listener = new Listener();

    for (var i = 0; i < data.length; i++) {
      _get((_thisSuper = _assertThisInitialized(_this), _getPrototypeOf(ArrayModel.prototype)), "push", _thisSuper).call(_thisSuper, vassilify(data[i]));
    }

    return _this;
  }
  /* Array members */

  /**
   * Gets the last value of array and null when it is empty
   * @return {?IValue}
   */


  _createClass(ArrayModel, [{
    key: "last",
    get: function get() {
      return this.length ? this[this.length - 1] : null;
    }
    /**
     * Calls Array.fill and notify about changes
     * @param value {*} value to fill with
     * @param start {?number} begin index
     * @param end {?number} end index
     * @return {ArrayModel} a pointer to this
     */

  }, {
    key: "fill",
    value: function fill(value, start, end) {
      if (!start) {
        start = 0;
      }

      if (!end) {
        end = this.length;
      }

      for (var i = start; i < end; i++) {
        if (this[i] instanceof _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_0__.IValue) {
          this[i].$ = value;
        }
      }

      return this;
    }
    /**
     * Calls Array.pop and notify about changes
     * @return {IValue} removed value
     */

  }, {
    key: "pop",
    value: function pop() {
      var v = _get(_getPrototypeOf(ArrayModel.prototype), "pop", this).call(this);

      if (v) {
        this.listener.emitRemoved(this.length, v);
      }

      return v;
    }
    /**
     * Calls Array.push and notify about changes
     * @param items {...IValue} values to push
     * @return {number} new length of array
     */

  }, {
    key: "push",
    value: function push() {
      for (var _len = arguments.length, items = new Array(_len), _key = 0; _key < _len; _key++) {
        items[_key] = arguments[_key];
      }

      for (var _i = 0, _items = items; _i < _items.length; _i++) {
        var item = _items[_i];
        var v = vassilify(item);
        this.listener.emitAdded(this.length, v);

        _get(_getPrototypeOf(ArrayModel.prototype), "push", this).call(this, v);
      }

      return this.length;
    }
    /**
     * Calls Array.shift and notify about changed
     * @return {IValue} the shifted value
     */

  }, {
    key: "shift",
    value: function shift() {
      var v = _get(_getPrototypeOf(ArrayModel.prototype), "shift", this).call(this);

      if (v) {
        this.listener.emitRemoved(0, v);
      }

      return v;
    }
    /**
     * Calls Array.splice and notify about changed
     * @param start {number} start index
     * @param deleteCount {?number} delete count
     * @param items {...IValue}
     * @return {ArrayModel} a pointer to this
     */

  }, {
    key: "splice",
    value: function splice(start, deleteCount) {
      var _get2;

      for (var _len2 = arguments.length, items = new Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
        items[_key2 - 2] = arguments[_key2];
      }

      start = Math.min(start, this.length);
      items = items ? items.map(function (v) {
        return vassilify(v);
      }) : [];
      deleteCount = deleteCount || this.length - start;

      for (var i = 0; i < deleteCount; i++) {
        var index = start + deleteCount - i - 1;

        if (this[index]) {
          this.listener.emitRemoved(index, this[index]);
        }
      }

      for (var _i2 = 0; _i2 < items.length; _i2++) {
        this.listener.emitAdded(start + _i2, items[_i2]);
      }

      return new ArrayModel((_get2 = _get(_getPrototypeOf(ArrayModel.prototype), "splice", this)).call.apply(_get2, [this, start, deleteCount].concat(_toConsumableArray(items))));
    }
    /* Vasile.js array interface */

    /**
     * Calls Array.unshift and notify about changed
     * @param items {...IValue} values to insert
     * @return {number | void} the length after prepend
     */

  }, {
    key: "unshift",
    value: function unshift() {
      var _get3;

      for (var _len3 = arguments.length, items = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
        items[_key3] = arguments[_key3];
      }

      items = items.map(function (v) {
        return vassilify(v);
      });

      for (var i = 0; i < items.length; i++) {
        this.listener.emitAdded(i, items[i]);
      }

      return (_get3 = _get(_getPrototypeOf(ArrayModel.prototype), "unshift", this)).call.apply(_get3, [this].concat(_toConsumableArray(items)));
    }
    /**
     * Inserts a value to the end of array
     * @param v {*} value to insert
     * @return {this} a pointer to this
     */

  }, {
    key: "append",
    value: function append(v) {
      v = vassilify(v);
      this.listener.emitAdded(this.length, v);

      _get(_getPrototypeOf(ArrayModel.prototype), "push", this).call(this, v);

      return this;
    }
    /**
     * Clears array
     * @return {this} a pointer to this
     */

  }, {
    key: "clear",
    value: function clear() {
      var _iterator3 = _createForOfIteratorHelper(this),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var v = _step3.value;
          this.listener.emitRemoved(0, v);
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }

      _get(_getPrototypeOf(ArrayModel.prototype), "splice", this).call(this, 0);

      return this;
    }
    /**
     * Inserts a value to position <i>index</i>
     * @param index {number} index to insert value
     * @param v {*} value to insert
     * @return {this} a pointer to this
     */

  }, {
    key: "insert",
    value: function insert(index, v) {
      v = vassilify(v);
      this.listener.emitAdded(index, v);

      _get(_getPrototypeOf(ArrayModel.prototype), "splice", this).call(this, index, 0, v);

      return this;
    }
    /**
     * Inserts a value to the beginning of array
     * @param v {*} value to insert
     * @return {this} a pointer to this
     */

  }, {
    key: "prepend",
    value: function prepend(v) {
      v = vassilify(v);
      this.listener.emitAdded(0, v);

      _get(_getPrototypeOf(ArrayModel.prototype), "unshift", this).call(this, v);

      return this;
    }
    /**
     * Removes a value from an index
     * @param index {number} index of value to remove
     * @return {this} a pointer to this
     */

  }, {
    key: "removeAt",
    value: function removeAt(index) {
      if (this[index]) {
        this.listener.emitRemoved(index, this[index]);

        _get(_getPrototypeOf(ArrayModel.prototype), "splice", this).call(this, index, 1);
      }

      return this;
    }
    /**
     * Removes the first value of array
     * @return {this} a pointer to this
     */

  }, {
    key: "removeFirst",
    value: function removeFirst() {
      if (this.length) {
        this.listener.emitRemoved(0, this[0]);

        _get(_getPrototypeOf(ArrayModel.prototype), "shift", this).call(this);
      }

      return this;
    }
    /**
     * Removes the ast value of array
     * @return {this} a pointer to this
     */

  }, {
    key: "removeLast",
    value: function removeLast() {
      if (this.last) {
        this.listener.emitRemoved(this.length - 1, this.last);

        _get(_getPrototypeOf(ArrayModel.prototype), "pop", this).call(this);
      }

      return this;
    }
    /**
     * Remove the first occurrence of value
     * @param v {IValue} value to remove
     * @return {this}
     */

  }, {
    key: "removeOne",
    value: function removeOne(v) {
      this.removeAt(this.indexOf(v));
      return this;
    }
  }]);

  return ArrayModel;
}( /*#__PURE__*/_wrapNativeSuper(Array));
/**
 * A Object based model
 * @extends Object<String, IValue>
 */

var ObjectModel = /*#__PURE__*/function (_Object) {
  _inherits(ObjectModel, _Object);

  var _super2 = _createSuper(ObjectModel);

  /**
   * the listener of object
   * @type {Listener}
   */

  /**
   * Constructs a object model from an object
   * @param obj {Object<String, IValue>} input data
   */
  function ObjectModel() {
    var _this2;

    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, ObjectModel);

    _this2 = _super2.call(this);
    _this2.listener = new Listener();

    var ts = _assertThisInitialized(_this2);

    for (var i in obj) {
      ts[i] = vassilify(obj[i]);
    }

    return _this2;
  }
  /**
   * Gets a value of a field
   * @param key {string}
   * @return {IValue<*>}
   */


  _createClass(ObjectModel, [{
    key: "get",
    value: function get(key) {
      var ts = this;
      return ts[key];
    }
    /**
     * Sets a object property value <b>(use for new properties only)</b>
     * @param key {string} property name
     * @param v {*} property value
     * @return {ObjectModel} a pointer to this
     */

  }, {
    key: "set",
    value: function set(key, v) {
      var ts = this;

      if (ts[key]) {
        this.listener.emitRemoved(key, ts[key]);
      }

      ts[key] = vassilify(v);
      this.listener.emitAdded(key, ts[key]);
      return this;
    }
    /**
     * Deletes a object property
     * @param key {string} property name
     */

  }, {
    key: "delete",
    value: function _delete(key) {
      var ts = this;

      if (ts[key]) {
        this.listener.emitRemoved(key, ts[key]);
        delete ts[key];
      }
    }
  }]);

  return ObjectModel;
}( /*#__PURE__*/_wrapNativeSuper(Object));
/**
 * A Map based memory
 * @extends Map<*, IValue>
 */

var MapModel = /*#__PURE__*/function (_Map) {
  _inherits(MapModel, _Map);

  var _super3 = _createSuper(MapModel);

  /**
   * listener of map
   * @type {Listener}
   */

  /**
   * Constructs a map model based on a map
   * @param map {Map<*, IValue>} input data
   */
  function MapModel() {
    var _thisSuper2, _this3;

    var map = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new Map();

    _classCallCheck(this, MapModel);

    _this3 = _super3.call(this);
    _this3.listener = new Listener();

    var _iterator4 = _createForOfIteratorHelper(map),
        _step4;

    try {
      for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
        var data = _step4.value;

        _get((_thisSuper2 = _assertThisInitialized(_this3), _getPrototypeOf(MapModel.prototype)), "set", _thisSuper2).call(_thisSuper2, data[0], vassilify(data[1]));
      }
    } catch (err) {
      _iterator4.e(err);
    } finally {
      _iterator4.f();
    }

    return _this3;
  }
  /**
   * Calls Map.clear and notify abut changes
   */


  _createClass(MapModel, [{
    key: "clear",
    value: function clear() {
      var _iterator5 = _createForOfIteratorHelper(this),
          _step5;

      try {
        for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
          var data = _step5.value;
          this.listener.emitRemoved(data[0], data[1]);
        }
      } catch (err) {
        _iterator5.e(err);
      } finally {
        _iterator5.f();
      }

      _get(_getPrototypeOf(MapModel.prototype), "clear", this).call(this);
    }
    /**
     * Calls Map.delete and notify abut changes
     * @param key {*} key
     * @return {boolean} true if removed something, otherwise false
     */

  }, {
    key: "delete",
    value: function _delete(key) {
      var tmp = _get(_getPrototypeOf(MapModel.prototype), "get", this).call(this, key);

      if (tmp) {
        this.listener.emitRemoved(key, tmp);
      }

      return _get(_getPrototypeOf(MapModel.prototype), "delete", this).call(this, key);
    }
    /**
     * Calls Map.set and notify abut changes
     * @param key {*} key
     * @param value {IValue} value
     * @return {MapModel} a pointer to this
     */

  }, {
    key: "set",
    value: function set(key, value) {
      var tmp = _get(_getPrototypeOf(MapModel.prototype), "get", this).call(this, key);

      if (tmp) {
        this.listener.emitRemoved(key, tmp);
      }

      var v = vassilify(value);

      _get(_getPrototypeOf(MapModel.prototype), "set", this).call(this, key, v);

      this.listener.emitAdded(key, v);
      return this;
    }
  }]);

  return MapModel;
}( /*#__PURE__*/_wrapNativeSuper(Map));
/**
 * A Set based model
 * @extends Set<IValue>
 */

var SetModel = /*#__PURE__*/function (_Set) {
  _inherits(SetModel, _Set);

  var _super4 = _createSuper(SetModel);

  /**
   * Constructs a set model based on a set
   * @param set {Set<IValue>} input data
   */
  function SetModel() {
    var _thisSuper3, _this4;

    var set = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new Set();

    _classCallCheck(this, SetModel);

    _this4 = _super4.call(this);
    _this4.listener = new Listener();

    var _iterator6 = _createForOfIteratorHelper(set),
        _step6;

    try {
      for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
        var item = _step6.value;

        _get((_thisSuper3 = _assertThisInitialized(_this4), _getPrototypeOf(SetModel.prototype)), "add", _thisSuper3).call(_thisSuper3, vassilify(item));
      }
    } catch (err) {
      _iterator6.e(err);
    } finally {
      _iterator6.f();
    }

    return _this4;
  }
  /**
   * Calls Set.add and notify abut changes
   * @param value {*} value
   * @return {this} a pointer to this
   */


  _createClass(SetModel, [{
    key: "add",
    value: function add(value) {
      value = vassilify(value);

      if (!_get(_getPrototypeOf(SetModel.prototype), "has", this).call(this, value)) {
        this.listener.emitAdded(null, value);

        _get(_getPrototypeOf(SetModel.prototype), "add", this).call(this, value);
      }

      return this;
    }
    /**
     * Calls Set.clear and notify abut changes
     */

  }, {
    key: "clear",
    value: function clear() {
      var _iterator7 = _createForOfIteratorHelper(this),
          _step7;

      try {
        for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
          var item = _step7.value;
          this.listener.emitRemoved(null, item);
        }
      } catch (err) {
        _iterator7.e(err);
      } finally {
        _iterator7.f();
      }

      _get(_getPrototypeOf(SetModel.prototype), "clear", this).call(this);
    }
    /**
     * Calls Set.delete and notify abut changes
     * @param value {IValue}
     * @return {boolean} true if a value was deleted, otherwise false
     */

  }, {
    key: "delete",
    value: function _delete(value) {
      if (_get(_getPrototypeOf(SetModel.prototype), "has", this).call(this, value)) {
        this.listener.emitRemoved(null, value);
      }

      return _get(_getPrototypeOf(SetModel.prototype), "delete", this).call(this, value);
    }
  }]);

  return SetModel;
}( /*#__PURE__*/_wrapNativeSuper(Set));
/**
 * Transforms a JS value to a Vasille.js value
 * @param v {*} input value
 * @return {IValue} transformed value
 */

function vassilify(v) {
  var ret;

  if (v instanceof _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_0__.IValue) {
    ret = v;
  } else if (Array.isArray(v)) {
    ret = new _value_js__WEBPACK_IMPORTED_MODULE_1__.Reference(new ArrayModel(v));
  } else if (v instanceof Map) {
    ret = new _value_js__WEBPACK_IMPORTED_MODULE_1__.Reference(new MapModel(v));
  } else if (v instanceof Set) {
    ret = new _value_js__WEBPACK_IMPORTED_MODULE_1__.Reference(new SetModel(v));
  } else if (v instanceof Object && v.constructor === Object) {
    ret = new _value_js__WEBPACK_IMPORTED_MODULE_1__.Reference(new ObjectModel(v));
  } else {
    ret = new _value_js__WEBPACK_IMPORTED_MODULE_1__.Reference(v);
  }

  return ret;
}

/***/ }),

/***/ "./node_modules/vasille-js/src/node.js":
/*!*********************************************!*\
  !*** ./node_modules/vasille-js/src/node.js ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TextNodePrivate": function() { return /* binding */ TextNodePrivate; },
/* harmony export */   "TextNode": function() { return /* binding */ TextNode; },
/* harmony export */   "BaseNodePrivate": function() { return /* binding */ BaseNodePrivate; },
/* harmony export */   "BaseNode": function() { return /* binding */ BaseNode; },
/* harmony export */   "TagNode": function() { return /* binding */ TagNode; },
/* harmony export */   "ExtensionNode": function() { return /* binding */ ExtensionNode; },
/* harmony export */   "UserNode": function() { return /* binding */ UserNode; },
/* harmony export */   "RepeatNodeItem": function() { return /* binding */ RepeatNodeItem; },
/* harmony export */   "SwitchedNodePrivate": function() { return /* binding */ SwitchedNodePrivate; },
/* harmony export */   "AppNode": function() { return /* binding */ AppNode; }
/* harmony export */ });
/* harmony import */ var _attribute_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./attribute.js */ "./node_modules/vasille-js/src/attribute.js");
/* harmony import */ var _bind_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bind.js */ "./node_modules/vasille-js/src/bind.js");
/* harmony import */ var _class_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./class.js */ "./node_modules/vasille-js/src/class.js");
/* harmony import */ var _executor_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./executor.js */ "./node_modules/vasille-js/src/executor.js");
/* harmony import */ var _interfaces_core_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./interfaces/core.js */ "./node_modules/vasille-js/src/interfaces/core.js");
/* harmony import */ var _interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./interfaces/errors.js */ "./node_modules/vasille-js/src/interfaces/errors.js");
/* harmony import */ var _interfaces_ibind_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./interfaces/ibind.js */ "./node_modules/vasille-js/src/interfaces/ibind.js");
/* harmony import */ var _interfaces_idefinition_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./interfaces/idefinition.js */ "./node_modules/vasille-js/src/interfaces/idefinition.js");
/* harmony import */ var _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./interfaces/ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
/* harmony import */ var _models_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./models.js */ "./node_modules/vasille-js/src/models.js");
/* harmony import */ var _style_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./style.js */ "./node_modules/vasille-js/src/style.js");
/* harmony import */ var _value_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./value.js */ "./node_modules/vasille-js/src/value.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }














/**
 * The private part of a text node
 */
var TextNodePrivate = /*#__PURE__*/function (_VasilleNodePrivate) {
  _inherits(TextNodePrivate, _VasilleNodePrivate);

  var _super = _createSuper(TextNodePrivate);

  function TextNodePrivate() {
    var _this;

    _classCallCheck(this, TextNodePrivate);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.value = void 0;
    _this.handler = void 0;
    return _this;
  }

  _createClass(TextNodePrivate, [{
    key: "preinitText",
    value:
    /**
     * Pre-initializes a text node
     * @param app {AppNode} the app node
     * @param rt {BaseNode} The root node
     * @param ts {BaseNode} The this node
     * @param before {?VasilleNode} node to paste after
     * @param text {String | IValue}
     */
    function preinitText(app, rt, ts, before, text) {
      _get(_getPrototypeOf(TextNodePrivate.prototype), "preinit", this).call(this, app, rt, ts, before);

      var value = text instanceof _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_8__.IValue ? text : new _value_js__WEBPACK_IMPORTED_MODULE_11__.Reference(text);
      var node = document.createTextNode(value.$);
      this.value = value;

      this.handler = function (v) {
        node.replaceData(0, -1, v.$);
      }.bind(null, value);

      value.on(this.handler);
      this.encapsulate(node);
      ts.$$appendChild(node, before);
    }
    /**
     * Clear node data
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      //$FlowFixMe
      this.value = null; //$FlowFixMe

      this.handler = null;

      _get(_getPrototypeOf(TextNodePrivate.prototype), "$destroy", this).call(this);
    }
  }]);

  return TextNodePrivate;
}(_interfaces_core_js__WEBPACK_IMPORTED_MODULE_4__.VasilleNodePrivate);
/**
 * Represents a text node
 */

var TextNode = /*#__PURE__*/function (_VasilleNode) {
  _inherits(TextNode, _VasilleNode);

  var _super2 = _createSuper(TextNode);

  /**
   * private data
   * @type {TextNodePrivate}
   */

  /**
   * Pointer to text node
   * @type {Text}
   */

  /**
   * Constructs a text node
   */
  function TextNode() {
    var _this2;

    _classCallCheck(this, TextNode);

    _this2 = _super2.call(this);
    _this2.$ = new TextNodePrivate();
    _this2.node = void 0;
    return _this2;
  }
  /**
   * Pre-initializes a text node
   * @param app {AppNode} the app node
   * @param rt {BaseNode} The root node
   * @param ts {BaseNode} The this node
   * @param before {?VasilleNode} node to paste after
   * @param text {String | IValue}
   */


  _createClass(TextNode, [{
    key: "$$preinitText",
    value: function $$preinitText(app, rt, ts, before, text) {
      this.$.preinitText(app, rt, ts, before, text);
      this.node = this.$.text;
    }
    /**
     * Runs garbage collector
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      this.$.$destroy();
      this.$ = null; //$FlowFixMe

      this.node = null;

      _get(_getPrototypeOf(TextNode.prototype), "$destroy", this).call(this);
    }
  }]);

  return TextNode;
}(_interfaces_core_js__WEBPACK_IMPORTED_MODULE_4__.VasilleNode);

/**
 * The private part of a base node
 */
var BaseNodePrivate = /*#__PURE__*/function (_VasilleNodePrivate2) {
  _inherits(BaseNodePrivate, _VasilleNodePrivate2);

  var _super3 = _createSuper(BaseNodePrivate);

  function BaseNodePrivate() {
    var _this3;

    _classCallCheck(this, BaseNodePrivate);

    for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    _this3 = _super3.call.apply(_super3, [this].concat(args));
    _this3.building = void 0;
    _this3["class"] = new Set();
    _this3.watch = new Set();
    _this3.signal = new Map();
    _this3.refs = new Map();
    _this3.slots = new Map();
    _this3.frozen = false;
    _this3.unmounted = false;
    _this3.onDestroy = void 0;
    return _this3;
  }

  _createClass(BaseNodePrivate, [{
    key: "rt",
    get:
    /**
     * Get the current root (ts on building, rt on filling)
     * @type {BaseNode}
     */
    function get() {
      return !this.building && _get(_getPrototypeOf(BaseNodePrivate.prototype), "root", this) instanceof BaseNode ? _get(_getPrototypeOf(BaseNodePrivate.prototype), "root", this) : this.ts;
    }
    /**
     * Garbage collection
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      var _iterator = _createForOfIteratorHelper(this["class"]),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var c = _step.value;
          c.$destroy();
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      this["class"].clear(); //$FlowFixMe

      this["class"] = null;

      var _iterator2 = _createForOfIteratorHelper(this.watch),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var w = _step2.value;
          w.$destroy();
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }

      this.watch.clear(); //$FlowFixMe

      this.watch = null;
      this.signal.clear(); //$FlowFixMe

      this.signal = null;

      var _iterator3 = _createForOfIteratorHelper(this.refs),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var ref = _step3.value;

          if (ref instanceof Set) {
            ref.clear();
          }
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }

      this.refs.clear(); //$FlowFixMe

      this.refs = null;
      this.slots.clear(); //$FlowFixMe

      this.slots = null;

      if (this.onDestroy) {
        this.onDestroy();
      }

      _get(_getPrototypeOf(BaseNodePrivate.prototype), "$destroy", this).call(this);
    }
  }]);

  return BaseNodePrivate;
}(_interfaces_core_js__WEBPACK_IMPORTED_MODULE_4__.VasilleNodePrivate);
/**
 * Represents an Vasille.js node which can contains children
 * @extends VasilleNode
 */

var BaseNode = /*#__PURE__*/function (_VasilleNode2) {
  _inherits(BaseNode, _VasilleNode2);

  var _super4 = _createSuper(BaseNode);

  /**
   * The children list
   * @type {Array<VasilleNode>}
   */

  /**
   * Constructs a base node
   * @param $ {?BaseNodePrivate}
   */
  function BaseNode($) {
    var _this4;

    _classCallCheck(this, BaseNode);

    _this4 = _super4.call(this, $ || new BaseNodePrivate());
    _this4.$children = [];
    return _this4;
  }
  /**
   * Pre-initializes a base node which can contain children
   * @param app {AppNode} the app node
   * @param rt {BaseNode} The root node
   * @param ts {BaseNode} The this node
   * @param before {?VasilleNode} node to paste after it
   * @param node {HTMLElement | Text | Comment} The encapsulated node
   */


  _createClass(BaseNode, [{
    key: "$$preinitNode",
    value: function $$preinitNode(app, rt, ts, before, node) {
      this.$.preinit(app, rt, ts, before);
      this.$.encapsulate(node);
      ts.$$appendChild(node, before);
    }
    /**
     * Start component building
     */

  }, {
    key: "$$startBuilding",
    value: function $$startBuilding() {
      this.$.slots.set("default", this);
      this.$.building = true;
    }
    /**
     * Stop component building
     */

  }, {
    key: "$$stopBuilding",
    value: function $$stopBuilding() {
      this.$.building = false;
      this.$mounted();
    }
    /**
     * Initialize node
     */

  }, {
    key: "$init",
    value: function $init() {
      this.$$startBuilding();
      this.$createAttrs();
      this.$createStyle();
      this.$createSignals();
      this.$createWatchers();
      this.$created();
      this.$createDom();
      this.$$stopBuilding();
    }
    /**
     * Assigns value to this property is such exists and is a IValue
     * @param ts {Object} pointer to this
     * @param prop {string} property name
     * @param value {*} value to assign
     */

  }, {
    key: "$$unsafeAssign",
    value: function $$unsafeAssign(ts, prop, value) {
      var field = ts[prop];

      if (!field || !(field instanceof _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_8__.IValue)) {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.notFound)("no such property: " + prop);
      }

      if (!field.type) {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.userError)("field ".concat(prop, " is private"), "private-field");
      }

      if (field instanceof _value_js__WEBPACK_IMPORTED_MODULE_11__.Pointer && field.value instanceof _value_js__WEBPACK_IMPORTED_MODULE_11__.Reference && value instanceof _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_8__.IValue) {
        field.$ = value;
      }

      if (value instanceof _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_8__.IValue) {
        ts[prop] = value;
      } else {
        field.$ = value;
      }
    }
    /** To be overloaded: created event handler */

  }, {
    key: "$created",
    value: function $created() {}
    /** To be overloaded: mounted event handler */

  }, {
    key: "$mounted",
    value: function $mounted() {}
    /** To be overloaded: ready event handler */

  }, {
    key: "$ready",
    value: function $ready() {}
    /** To be overloaded: attributes creation milestone */

  }, {
    key: "$createAttrs",
    value: function $createAttrs() {}
    /**
     * Runs garbage collector
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      var $ = this.$;

      if ($.root instanceof BaseNode) {
        var _iterator4 = _createForOfIteratorHelper($.root.$.refs),
            _step4;

        try {
          for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
            var it = _step4.value;
            var ref = it[1];

            if (ref === this) {
              $.root.$.refs["delete"](it[0]);
            } else if (ref instanceof Set && ref.has(this)) {
              ref["delete"](this);
            }
          }
        } catch (err) {
          _iterator4.e(err);
        } finally {
          _iterator4.f();
        }
      }

      var _iterator5 = _createForOfIteratorHelper(this.$children),
          _step5;

      try {
        for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
          var child = _step5.value;
          child.$destroy();
        }
      } catch (err) {
        _iterator5.e(err);
      } finally {
        _iterator5.f();
      }

      $.$destroy();
      this.$ = null;
      this.$children.splice(0); //$FlowFixMe

      this.$children = null;

      _get(_getPrototypeOf(BaseNode.prototype), "$destroy", this).call(this);
    }
    /** To be overloaded: $style attributes creation milestone */

  }, {
    key: "$createStyle",
    value: function $createStyle() {}
    /** To be overloaded: signals creation milestone */

  }, {
    key: "$createSignals",
    value: function $createSignals() {}
    /** To be overloaded: watchers creation milestone */

  }, {
    key: "$createWatchers",
    value: function $createWatchers() {}
    /** To be overloaded: DOM creation milestone */

  }, {
    key: "$createDom",
    value: function $createDom() {}
    /**
     * create a private field
     * @param value {*}
     * @return {IValue<*>}
     */

  }, {
    key: "$private",
    value: function $private(value) {
      var ret = (0,_models_js__WEBPACK_IMPORTED_MODULE_9__.vassilify)(value);
      this.$.watch.add(ret);
      return ret;
    }
    /**
     * creates a publis field
     * @param type {Function}
     * @param value {*}
     * @return {Reference}
     */

  }, {
    key: "$public",
    value: function $public(type) {
      var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (!(0,_interfaces_idefinition_js__WEBPACK_IMPORTED_MODULE_7__.checkType)(value, type) || value instanceof _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_8__.IValue) {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.typeError)("wrong initial public field value");
      }

      var ret = (0,_models_js__WEBPACK_IMPORTED_MODULE_9__.vassilify)(value);

      if (ret instanceof _value_js__WEBPACK_IMPORTED_MODULE_11__.Reference) {
        ret.type = type;
        this.$.watch.add(ret);
        return ret;
      } else {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.internalError)("Something goes wrong :(");
      }
    }
    /**
     * creates a pointer
     * @param type {Function}
     * @return {Pointer}
     */

  }, {
    key: "$pointer",
    value: function $pointer(type) {
      var ref = new _value_js__WEBPACK_IMPORTED_MODULE_11__.Reference();
      var pointer = new _value_js__WEBPACK_IMPORTED_MODULE_11__.Pointer(ref);
      ref.type = type;
      this.$.watch.add(ref);
      this.$.watch.add(pointer);
      return pointer;
    }
    /**
     * Gets a attribute value
     * @param name {string} Attribute name
     * @return {IValue<string>}
     */

  }, {
    key: "$attr",
    value: function $attr(name) {
      return this.$.attr(name);
    }
    /**
     * Defines a attribute
     * @param name {String} The name of attribute
     * @param value {String | IValue | Callable} A $$value or a $$value getter
     * @return {BaseNode} A pointer to this
     */

  }, {
    key: "$defAttr",
    value: function $defAttr(name, value) {
      if (value instanceof _interfaces_idefinition_js__WEBPACK_IMPORTED_MODULE_7__.Callable) {
        this.$.$attrs[name] = (0,_attribute_js__WEBPACK_IMPORTED_MODULE_0__.attributify)(this.$.rt, this, name, null, value);
      } else {
        this.$.$attrs[name] = (0,_attribute_js__WEBPACK_IMPORTED_MODULE_0__.attributify)(this.$.rt, this, name, value);
      }

      return this;
    }
    /**
     * Defines a set of attributes
     * @param obj {Object<String, String | IValue>} A set attributes
     * @return {BaseNode} A pointer to this
     */

  }, {
    key: "$defAttrs",
    value: function $defAttrs(obj) {
      for (var i in obj) {
        this.$.$attrs[i] = (0,_attribute_js__WEBPACK_IMPORTED_MODULE_0__.attributify)(this.$.rt, this, i, obj[i]);
      }

      return this;
    }
    /**
     * Creates and binds a multivalued binding to attribute
     * @param name {String} The name of attribute
     * @param calculator {Function} Binding calculator (must return a value)
     * @param values {...IValue} Values to bind
     * @return {BaseNode} A pointer to this
     */

  }, {
    key: "$bindAttr",
    value: function $bindAttr(name, calculator) {
      for (var _len3 = arguments.length, values = new Array(_len3 > 2 ? _len3 - 2 : 0), _key3 = 2; _key3 < _len3; _key3++) {
        values[_key3 - 2] = arguments[_key3];
      }

      this.$.$attrs[name] = _construct(_attribute_js__WEBPACK_IMPORTED_MODULE_0__.AttributeBinding, [this.$.rt, this, name, calculator].concat(values));
      return this;
    }
    /**
     * Sets a attribute value
     * @param name {string} Name of attribute
     * @param value {string} Reference of attribute
     * @return {BaseNode} A pointer to this
     */

  }, {
    key: "$setAttr",
    value: function $setAttr(name, value) {
      this.$.app.$run.setAttribute(this.$.el, name, value);
      return this;
    }
    /**
     * Sets value of some attributes
     * @param data {Object<string, string>} Names and values of attributes
     * @return {BaseNode} A pointer to this
     */

  }, {
    key: "$setAttrs",
    value: function $setAttrs(data) {
      for (var i in data) {
        this.$.app.$run.setAttribute(this.$.el, i, data[i]);
      }

      return this;
    }
    /**
     * Adds a CSS class
     * @param cl {string} Class name
     * @return {BaseNode} A pointer to this
     */

  }, {
    key: "$addClass",
    value: function $addClass(cl) {
      this.$.el.classList.add(cl);
      return this;
    }
    /**
     * Adds some CSS classes
     * @param cl {...string} Classes names
     * @return {BaseNode} A pointer to this
     */

  }, {
    key: "$addClasses",
    value: function $addClasses() {
      var _this$$$el$classList;

      (_this$$$el$classList = this.$.el.classList).add.apply(_this$$$el$classList, arguments);

      return this;
    }
    /**
     * Bind a CSS class
     * @param cl {?string}
     * @param value {string | IValue | null}
     * @param func {?Callable}
     * @return {BaseNode}
     */

  }, {
    key: "$bindClass",
    value: function $bindClass(cl) {
      var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var func = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      this.$["class"].add((0,_class_js__WEBPACK_IMPORTED_MODULE_2__.classify)(this.$.rt, this, cl || "", value, func));
      return this;
    }
    /**
     * Gets a style attribute value
     * @param name {string} Name of style attribute
     * @return {IValue}
     */

  }, {
    key: "$style",
    value: function $style(name) {
      return this.$.style(name);
    }
    /**
     * Defines a style attribute
     * @param name {String} The name of style attribute
     * @param value {String | IValue | Callable} A value or a value getter
     * @return {this} A pointer to this
     */

  }, {
    key: "$defStyle",
    value: function $defStyle(name, value) {
      if (value instanceof _interfaces_idefinition_js__WEBPACK_IMPORTED_MODULE_7__.Callable) {
        this.$.$style[name] = (0,_style_js__WEBPACK_IMPORTED_MODULE_10__.stylify)(this.$.rt, this, name, null, value);
      } else {
        this.$.$style[name] = (0,_style_js__WEBPACK_IMPORTED_MODULE_10__.stylify)(this.$.rt, this, name, value);
      }

      return this;
    }
    /**
     * Defines a set of style attributes
     * @param obj {Object<String, String | IValue>} A set of style attributes
     * @return {this} A pointer to this
     */

  }, {
    key: "$defStyles",
    value: function $defStyles(obj) {
      for (var i in obj) {
        this.$.$style[i] = (0,_style_js__WEBPACK_IMPORTED_MODULE_10__.stylify)(this.$.rt, this, i, obj[i]);
      }

      return this;
    }
    /**
     * Creates and binds a calculator to a style attribute
     * @param name {String} Name of style attribute
     * @param calculator {Function} A calculator for style value
     * @param values {...IValue} Values to bind
     * @return {this} A pointer to this
     */

  }, {
    key: "$bindStyle",
    value: function $bindStyle(name, calculator) {
      for (var _len4 = arguments.length, values = new Array(_len4 > 2 ? _len4 - 2 : 0), _key4 = 2; _key4 < _len4; _key4++) {
        values[_key4 - 2] = arguments[_key4];
      }

      this.$.$style[name] = _construct(_style_js__WEBPACK_IMPORTED_MODULE_10__.StyleBinding, [this.$.rt, this, name, calculator].concat(values));
      return this;
    }
    /**
     * Sets a style property value
     * @param prop {string} Property name
     * @param value {string} Property value
     * @return {BaseNode}
     */

  }, {
    key: "$setStyle",
    value: function $setStyle(prop, value) {
      this.$.app.$run.setStyle(this.$.el, prop, value);
      return this;
    }
    /**
     * Sets some style property value
     * @param data {Object<string, string>} Names and value of properties
     * @return {BaseNode}
     */

  }, {
    key: "$setStyles",
    value: function $setStyles(data) {
      for (var i in data) {
        this.$.app.$run.setStyle(this.$.el, i, data[i]);
      }

      return this;
    }
    /**
     * Defines a signal
     * @param name {string} Signal name
     * @param types {...Function} Arguments types
     */

  }, {
    key: "$defSignal",
    value: function $defSignal(name) {
      for (var _len5 = arguments.length, types = new Array(_len5 > 1 ? _len5 - 1 : 0), _key5 = 1; _key5 < _len5; _key5++) {
        types[_key5 - 1] = arguments[_key5];
      }

      this.$.signal.set(name, {
        args: types,
        handlers: []
      });
    }
    /**
     * Add a handler for a signal
     * @param name {string} Signal name
     * @param func {Function} Handler
     */

  }, {
    key: "$on",
    value: function $on(name, func) {
      var signal = this.$.signal.get(name);

      if (!signal) {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.notFound)("no such signal: " + name);
      }

      signal.handlers.push(func);
    }
    /**
     * Emit a signal
     * @param name {string} Signal name
     * @param args {...*} Signal arguments
     */

  }, {
    key: "$emit",
    value: function $emit(name) {
      var signal = this.$.signal.get(name);

      if (!signal) {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.notFound)("no such signal: " + name);
      }

      for (var _len6 = arguments.length, args = new Array(_len6 > 1 ? _len6 - 1 : 0), _key6 = 1; _key6 < _len6; _key6++) {
        args[_key6 - 1] = arguments[_key6];
      }

      var compatible = args.length === signal.args.length;

      if (compatible && this.$.app.$debug) {
        for (var i = 0; i < args.length; i++) {
          if (!(0,_interfaces_idefinition_js__WEBPACK_IMPORTED_MODULE_7__.checkType)(args[i], signal.args[i])) {
            compatible = false;
          }
        }
      }

      if (!compatible) {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.typeError)("incompatible signals arguments");
      }

      var _iterator6 = _createForOfIteratorHelper(signal.handlers),
          _step6;

      try {
        for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
          var handler = _step6.value;

          try {
            handler.apply(void 0, args);
          } catch (e) {
            console.error("Vasille.js: Handler throw exception at ".concat(this.constructor.name, "::").concat(name, ": "), e);
          }
        }
      } catch (err) {
        _iterator6.e(err);
      } finally {
        _iterator6.f();
      }
    }
    /**
     * Add a listener for an event
     * @param name {string} Event name
     * @param handler {function (Event)} Event handler
     * @param options {Object | boolean} addEventListener options
     * @return {this}
     */

  }, {
    key: "$listen",
    value: function $listen(name, handler, options) {
      this.$.el.addEventListener(name, handler, options || {});
      return this;
    }
    /**
     * @param handler {function (MouseEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenContextMenu",
    value: function $listenContextMenu(handler, options) {
      this.$listen("contextmenu", handler, options);
    }
    /**
     * @param handler {function (MouseEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenMouseDown",
    value: function $listenMouseDown(handler, options) {
      this.$listen("mousedown", handler, options);
    }
    /**
     * @param handler {function (MouseEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenMouseEnter",
    value: function $listenMouseEnter(handler, options) {
      this.$listen("mouseenter", handler, options);
    }
    /**
     * @param handler {function (MouseEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenMouseLeave",
    value: function $listenMouseLeave(handler, options) {
      this.$listen("mouseleave", handler, options);
    }
    /**
     * @param handler {function (MouseEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenMouseMove",
    value: function $listenMouseMove(handler, options) {
      this.$listen("mousemove", handler, options);
    }
    /**
     * @param handler {function (MouseEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenMouseOut",
    value: function $listenMouseOut(handler, options) {
      this.$listen("mouseout", handler, options);
    }
    /**
     * @param handler {function (MouseEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenMouseOver",
    value: function $listenMouseOver(handler, options) {
      this.$listen("mouseover", handler, options);
    }
    /**
     * @param handler {function (MouseEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenMouseUp",
    value: function $listenMouseUp(handler, options) {
      this.$listen("mouseup", handler, options);
    }
    /**
     * @param handler {function (MouseEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenClick",
    value: function $listenClick(handler, options) {
      this.$listen("click", handler, options);
    }
    /**
     * @param handler {function (MouseEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenDblClick",
    value: function $listenDblClick(handler, options) {
      this.$listen("dblclick", handler, options);
    }
    /**
     * @param handler {function (FocusEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenBlur",
    value: function $listenBlur(handler, options) {
      this.$listen("blur", handler, options);
    }
    /**
     * @param handler {function (FocusEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenFocus",
    value: function $listenFocus(handler, options) {
      this.$listen("focus", handler, options);
    }
    /**
     * @param handler {function (FocusEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenFocusIn",
    value: function $listenFocusIn(handler, options) {
      this.$listen("focusin", handler, options);
    }
    /**
     * @param handler {function (FocusEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenFocusOut",
    value: function $listenFocusOut(handler, options) {
      this.$listen("focusout", handler, options);
    }
    /**
     * @param handler {function (KeyboardEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenKeyDown",
    value: function $listenKeyDown(handler, options) {
      this.$listen("keydown", handler, options);
    }
    /**
     * @param handler {function (KeyboardEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenKeyUp",
    value: function $listenKeyUp(handler, options) {
      this.$listen("keyup", handler, options);
    }
    /**
     * @param handler {function (KeyboardEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenKeyPress",
    value: function $listenKeyPress(handler, options) {
      this.$listen("keypress", handler, options);
    }
    /**
     * @param handler {function (TouchEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenTouchStart",
    value: function $listenTouchStart(handler, options) {
      this.$listen("touchstart", handler, options);
    }
    /**
     * @param handler {function (TouchEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenTouchMove",
    value: function $listenTouchMove(handler, options) {
      this.$listen("touchmove", handler, options);
    }
    /**
     * @param handler {function (TouchEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenTouchEnd",
    value: function $listenTouchEnd(handler, options) {
      this.$listen("touchend", handler, options);
    }
    /**
     * @param handler {function (TouchEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenTouchCancel",
    value: function $listenTouchCancel(handler, options) {
      this.$listen("touchcancel", handler, options);
    }
    /**
     * @param handler {function (WheelEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenWheel",
    value: function $listenWheel(handler, options) {
      this.$listen("wheel", handler, options);
    }
    /**
     * @param handler {function (ProgressEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenAbort",
    value: function $listenAbort(handler, options) {
      this.$listen("abort", handler, options);
    }
    /**
     * @param handler {function (ProgressEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenError",
    value: function $listenError(handler, options) {
      this.$listen("error", handler, options);
    }
    /**
     * @param handler {function (ProgressEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenLoad",
    value: function $listenLoad(handler, options) {
      this.$listen("load", handler, options);
    }
    /**
     * @param handler {function (ProgressEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenLoadEnd",
    value: function $listenLoadEnd(handler, options) {
      this.$listen("loadend", handler, options);
    }
    /**
     * @param handler {function (ProgressEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenLoadStart",
    value: function $listenLoadStart(handler, options) {
      this.$listen("loadstart", handler, options);
    }
    /**
     * @param handler {function (ProgressEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenProgress",
    value: function $listenProgress(handler, options) {
      this.$listen("progress", handler, options);
    }
    /**
     * @param handler {function (ProgressEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenTimeout",
    value: function $listenTimeout(handler, options) {
      this.$listen("timeout", handler, options);
    }
    /**
     * @param handler {function (DragEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenDrag",
    value: function $listenDrag(handler, options) {
      this.$listen("drag", handler, options);
    }
    /**
     * @param handler {function (DragEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenDragEnd",
    value: function $listenDragEnd(handler, options) {
      this.$listen("dragend", handler, options);
    }
    /**
     * @param handler {function (DragEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenDragEnter",
    value: function $listenDragEnter(handler, options) {
      this.$listen("dragenter", handler, options);
    }
    /**
     * @param handler {function (DragEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenDragExit",
    value: function $listenDragExit(handler, options) {
      this.$listen("dragexit", handler, options);
    }
    /**
     * @param handler {function (DragEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenDragLeave",
    value: function $listenDragLeave(handler, options) {
      this.$listen("dragleave", handler, options);
    }
    /**
     * @param handler {function (DragEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenDragOver",
    value: function $listenDragOver(handler, options) {
      this.$listen("dragover", handler, options);
    }
    /**
     * @param handler {function (DragEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenDragStart",
    value: function $listenDragStart(handler, options) {
      this.$listen("dragstart", handler, options);
    }
    /**
     * @param handler {function (DragEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenDrop",
    value: function $listenDrop(handler, options) {
      this.$listen("drop", handler, options);
    }
    /**
     * @param handler {function (PointerEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenPointerOver",
    value: function $listenPointerOver(handler, options) {
      this.$listen("pointerover", handler, options);
    }
    /**
     * @param handler {function (PointerEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenPointerEnter",
    value: function $listenPointerEnter(handler, options) {
      this.$listen("pointerenter", handler, options);
    }
    /**
     * @param handler {function (PointerEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenPointerDown",
    value: function $listenPointerDown(handler, options) {
      this.$listen("pointerdown", handler, options);
    }
    /**
     * @param handler {function (PointerEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenPointerMove",
    value: function $listenPointerMove(handler, options) {
      this.$listen("pointermove", handler, options);
    }
    /**
     * @param handler {function (PointerEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenPointerUp",
    value: function $listenPointerUp(handler, options) {
      this.$listen("pointerup", handler, options);
    }
    /**
     * @param handler {function (PointerEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenPointerCancel",
    value: function $listenPointerCancel(handler, options) {
      this.$listen("pointercancel", handler, options);
    }
    /**
     * @param handler {function (PointerEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenPointerOut",
    value: function $listenPointerOut(handler, options) {
      this.$listen("pointerout", handler, options);
    }
    /**
     * @param handler {function (PointerEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenPointerLeave",
    value: function $listenPointerLeave(handler, options) {
      this.$listen("pointerleave", handler, options);
    }
    /**
     * @param handler {function (PointerEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenGotPointerCapture",
    value: function $listenGotPointerCapture(handler, options) {
      this.$listen("gotpointercapture", handler, options);
    }
    /**
     * @param handler {function (PointerEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenLostPointerCapture",
    value: function $listenLostPointerCapture(handler, options) {
      this.$listen("lostpointercapture", handler, options);
    }
    /**
     * @param handler {function (AnimationEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenAnimationStart",
    value: function $listenAnimationStart(handler, options) {
      this.$listen("animationstart", handler, options);
    }
    /**
     * @param handler {function (AnimationEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenAnimationEnd",
    value: function $listenAnimationEnd(handler, options) {
      this.$listen("animationend", handler, options);
    }
    /**
     * @param handler {function (AnimationEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenAnimationIteraton",
    value: function $listenAnimationIteraton(handler, options) {
      this.$listen("animationiteration", handler, options);
    }
    /**
     * @param handler {function (ClipboardEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenClipboardChange",
    value: function $listenClipboardChange(handler, options) {
      this.$listen("clipboardchange", handler, options);
    }
    /**
     * @param handler {function (ClipboardEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenCut",
    value: function $listenCut(handler, options) {
      this.$listen("cut", handler, options);
    }
    /**
     * @param handler {function (ClipboardEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenCopy",
    value: function $listenCopy(handler, options) {
      this.$listen("copy", handler, options);
    }
    /**
     * @param handler {function (ClipboardEvent)}
     * @param options {Object | boolean}
     */

  }, {
    key: "$listenPaste",
    value: function $listenPaste(handler, options) {
      this.$listen("paste", handler, options);
    }
    /**
     * Defines a watcher
     * @param func {function} Function to run on value change
     * @param vars {...IValue} Values to listen
     */

  }, {
    key: "$defWatcher",
    value: function $defWatcher(func) {
      for (var _len7 = arguments.length, vars = new Array(_len7 > 1 ? _len7 - 1 : 0), _key7 = 1; _key7 < _len7; _key7++) {
        vars[_key7 - 1] = arguments[_key7];
      }

      if (vars.length === 0) {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.wrongBinding)("a watcher must be bound to a value at last");
      }

      this.$.watch.add(new _bind_js__WEBPACK_IMPORTED_MODULE_1__.Expression(func, vars, !this.freezed));
    }
    /**
     * Creates a bind expression
     * @param f {Function} function to alc expression value
     * @param args {...IValue} value sto bind
     * @return {IBind}
     */

  }, {
    key: "$bind",
    value: function $bind(f) {
      var res;

      for (var _len8 = arguments.length, args = new Array(_len8 > 1 ? _len8 - 1 : 0), _key8 = 1; _key8 < _len8; _key8++) {
        args[_key8 - 1] = arguments[_key8];
      }

      if (args.length === 0) {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.wrongBinding)("no values to bind");
      } else {
        res = new _bind_js__WEBPACK_IMPORTED_MODULE_1__.Expression(f, args, !this.freezed);
      }

      this.$.watch.add(res);
      return res;
    }
  }, {
    key: "$runOnDestroy",
    value: function $runOnDestroy(f) {
      this.$.onDestroy = f;
    }
    /**
     * Register current node as named slot
     * @param name {String} The name of slot
     */

  }, {
    key: "$makeSlot",
    value: function $makeSlot(name) {
      if (this.$.rt instanceof BaseNode) {
        this.$.rt.$.slots.set(name, this);
      }

      return this;
    }
    /**
     * Gets a slot by name
     * @param name {string} Name of slot
     * @return {BaseNode}
     */

  }, {
    key: "$slot",
    value: function $slot(name) {
      var node = this.$.slots.get(name);

      if (node instanceof BaseNode) {
        return node;
      }

      throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.notFound)("no such slot: " + name);
    }
    /**
     * Pushes a node to children immediately
     * @param node {VasilleNode} A node to push
     * @private
     */

  }, {
    key: "$$pushNode",
    value: function $$pushNode(node) {
      var lastChild = null;

      if (this.$children.length) {
        lastChild = this.$children[this.$children.length - 1];
      }

      if (lastChild) {
        lastChild.$.next = node;
      }

      node.$.prev = lastChild;
      node.$.parent = this;
      this.$children.push(node);
    }
    /**
     * Find first core node in shadow element if so exists
     * @param node {ExtensionNode} Node to iterate
     * @return {?CoreEl}
     */

  }, {
    key: "$$findFirstChild",
    value: function $$findFirstChild(node) {
      var _iterator7 = _createForOfIteratorHelper(node.$children),
          _step7;

      try {
        for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
          var child = _step7.value;
          if (child.$.unmounted) continue;

          if (child instanceof ExtensionNode) {
            var first = this.$$findFirstChild(child);

            if (first) {
              return first;
            }
          } else if (child instanceof TagNode || child instanceof TextNode) {
            return child.$.$el;
          }
        }
      } catch (err) {
        _iterator7.e(err);
      } finally {
        _iterator7.f();
      }
    }
    /**
     * Append a child in correct parent (to be overwritten)
     * @param node {HTMLElement | Text | Comment} A node to push
     * @param before {VasilleNode} node to paste after
     * @private
     */

  }, {
    key: "$$appendChild",
    value: function $$appendChild(node, before) {
      var $ = this.$;
      before = before || $.next;

      while (before && before.$.unmounted) {
        before = before.$.next;
      } // If we are inserting before a element node


      if (before instanceof TagNode) {
        $.app.$run.insertBefore($.el, node, before.$.el);
        return;
      } // If we are inserting before a shadow node


      if (before instanceof ExtensionNode) {
        var beforeNode = this.$$findFirstChild(before);

        if (beforeNode) {
          $.app.$run.insertBefore($.el, node, beforeNode);
          return;
        }
      } // If we are inserting in a shadow node or uninitiated element node


      if (this instanceof ExtensionNode && !($.parent instanceof AppNode) || this instanceof TagNode && (!$.el || $.el === node)) {
        $.parent.$$appendChild(node, $.next);
        return;
      } // If we have no more variants


      $.app.$run.appendChild($.el, node);
    }
    /**
     * Disable/Enable reactivity of component with feedback
     * @param cond {IValue} show condition
     * @param onOff {Function} on show feedback
     * @param onOn {Function} on hide feedback
     */

  }, {
    key: "$bindFreeze",
    value: function $bindFreeze(cond, onOff, onOn, onOffAfter, onOnAfter) {
      var $ = this.$;

      if ($.watch.has(cond)) {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.wrongBinding)(":show must be bound to an external component");
      }

      var expr = null;
      expr = new _bind_js__WEBPACK_IMPORTED_MODULE_1__.Expression(function (cond) {
        $.frozen = !cond;

        if (cond) {
          onOn === null || onOn === void 0 ? void 0 : onOn();

          var _iterator8 = _createForOfIteratorHelper($.watch),
              _step8;

          try {
            for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
              var watcher = _step8.value;

              if (watcher instanceof _interfaces_ibind_js__WEBPACK_IMPORTED_MODULE_6__.IBind) {
                watcher.link();
              }
            }
          } catch (err) {
            _iterator8.e(err);
          } finally {
            _iterator8.f();
          }

          onOnAfter === null || onOnAfter === void 0 ? void 0 : onOnAfter();
        } else {
          onOff === null || onOff === void 0 ? void 0 : onOff();

          var _iterator9 = _createForOfIteratorHelper($.watch),
              _step9;

          try {
            for (_iterator9.s(); !(_step9 = _iterator9.n()).done;) {
              var _watcher = _step9.value;

              if (_watcher instanceof _interfaces_ibind_js__WEBPACK_IMPORTED_MODULE_6__.IBind && _watcher !== expr) {
                _watcher.unlink();
              }
            }
          } catch (err) {
            _iterator9.e(err);
          } finally {
            _iterator9.f();
          }

          onOffAfter === null || onOffAfter === void 0 ? void 0 : onOffAfter();
        }
      }, [cond]);
      $.watch.add(expr);
    }
    /**
     * A v-show & ngShow alternative
     * @param cond {IValue} show condition
     */

  }, {
    key: "$bindShow",
    value: function $bindShow(cond) {
      var $ = this.$;
      var lastDisplay = $.el.style.display;
      return this.$bindFreeze(cond, function () {
        $.el.style.display = 'none';
      }, function () {
        $.el.style.display = lastDisplay;
      });
    }
    /**
     * Mount/Unmount a node
     * @param cond {IValue} show condition
     */

  }, {
    key: "$bindMount",
    value: function $bindMount(cond) {
      var $ = this.$;
      return this.$bindFreeze(cond, function () {
        $.unmounted = true;
      }, function () {
        $.unmounted = false;
      });
    }
    /**
     * Enable/Disable reactivity o component
     * @param cond {IValue} show condition
     */

  }, {
    key: "$bindAlive",
    value: function $bindAlive(cond) {
      return this.$bindFreeze(cond);
    }
    /**
     * Defines a text fragment
     * @param text {String | IValue} A text fragment string
     * @param cb {?function (TextNode)} Callback if previous is slot name
     * @return {BaseNode} A pointer to this
     */

  }, {
    key: "$defText",
    value: function $defText(text, cb) {
      var $ = this.$;
      var default_ = $.slots.get("default");

      if (default_ && default_ !== this && !$.building) {
        default_.$defText(text, cb);
        return this;
      }

      var node = new TextNode();
      node.$$preinitText($.app, $.rt, this, null, text);
      this.$$pushNode(node);

      if (cb) {
        $.app.$run.callCallback(function () {
          cb(node);
        });
      }

      return this;
    }
    /**
     * Defines a tag element
     * @param tagName {String} is the tag name
     * @param cb {function(TagNode, *)} Callback if previous is slot name
     * @return {BaseNode} A pointer to this
     */

  }, {
    key: "$defTag",
    value: function $defTag(tagName, cb) {
      var $ = this.$;
      var default_ = $.slots.get("default");

      if (default_ && default_ !== this && !$.building) {
        default_.$defTag(tagName, cb);
        return this;
      }

      var node = new TagNode();
      node.$.parent = this;
      node.$$preinitElementNode($.app, $.rt, this, null, tagName);
      node.$init();
      this.$$pushNode(node);
      $.app.$run.callCallback(function () {
        if (cb) {
          cb(node);
        }

        node.$ready();
      });
      return this;
    }
    /**
     * Defines a custom element
     * @param node {*} Custom element constructor
     * @param props {function(BaseNode)} List of properties values
     * @param cb {?function(BaseNode, *)} Callback if previous is slot name
     * @return {BaseNode} A pointer to this
     */

  }, {
    key: "$defElement",
    value: function $defElement(node, props, cb) {
      var $ = this.$;
      var default_ = $.slots.get("default");

      if (default_ && default_ !== this && !$.building) {
        default_.$defElement(node, props, cb);
        return this;
      }

      if (node instanceof _interfaces_core_js__WEBPACK_IMPORTED_MODULE_4__.VasilleNode) {
        node.$.parent = this;
      }

      if (node instanceof ExtensionNode) {
        node.$$preinitShadow($.app, $.rt, this, null);
      } else if (node instanceof TagNode || node instanceof TextNode) {
        node.$.preinit($.app, $.rt, this, null);
      }

      this.$$callPropsCallback(node, props);

      if (node instanceof _interfaces_core_js__WEBPACK_IMPORTED_MODULE_4__.VasilleNode) {
        this.$$pushNode(node);
      }

      $.app.$run.callCallback(function () {
        if (cb) {
          cb(node);
        }

        if (node instanceof BaseNode) {
          node.$ready();
        }
      });
      return this;
    }
    /**
     * Calls callback function to create properties
     * @param node {BaseNode}
     * @param props {function(BaseNode)}
     * @return {*}
     */

  }, {
    key: "$$callPropsCallback",
    value: function $$callPropsCallback(node, props) {
      if (node instanceof BaseNode) {
        var obj = {
          $bind: function $bind() {
            return node.$bind.apply(node, arguments);
          }
        };

        var _loop = function _loop(i) {
          if (node.hasOwnProperty(i)) {
            Object.defineProperty(obj, i, {
              configurable: false,
              enumerable: false,
              set: function set(value) {
                node.$$unsafeAssign(node, i, value);
              },
              get: function get() {
                return node[i];
              }
            });
          }
        };

        for (var i in node) {
          _loop(i);
        } //$FlowFixMe[incompatible-call]


        props(obj);
        node.$init();
      }
    }
    /**
     * Defines a repeater node
     * @param nodeT {RepeatNode} A repeat node object
     * @param props {function(BaseNode)} Send data to repeat node
     * @param cb {function(RepeatNodeItem, *)} Call-back to create child nodes
     * @return {BaseNode}
     */

  }, {
    key: "$defRepeater",
    value: function $defRepeater(nodeT, props, cb) {
      var $ = this.$;
      var default_ = $.slots.get("default");

      if (default_ && default_ !== this && !$.building) {
        default_.$defRepeater(nodeT, props, cb);
        return this;
      } //$FlowFixMe[incompatible-type]


      var node = nodeT;
      node.$.parent = this;
      node.$$preinitShadow($.app, this.$.rt, this, null);
      this.$$callPropsCallback(nodeT, props);
      this.$$pushNode(node);
      node.setCallback(cb);
      $.app.$run.callCallback(function () {
        node.$ready();
      });
      return this;
    }
    /**
     * Defines a if node
     * @param cond {* | IValue<*>} condition
     * @param cb {function(RepeatNodeItem, ?number)} Call-back to create child nodes
     * @return {this}
     */

  }, {
    key: "$defIf",
    value: function $defIf(cond, cb) {
      return this.$defSwitch({
        cond: cond,
        cb: cb
      });
    }
    /**
     * Defines a if-else node
     * @param ifCond {* | IValue<*>} `if` condition
     * @param ifCb {function(RepeatNodeItem, ?number)} Call-back to create `if` child nodes
     * @param elseCb {function(RepeatNodeItem, ?number)} Call-back to create `else` child nodes
     * @return {this}
     */

  }, {
    key: "$defIfElse",
    value: function $defIfElse(ifCond, ifCb, elseCb) {
      return this.$defSwitch({
        cond: ifCond,
        cb: ifCb
      }, {
        cond: true,
        cb: elseCb
      });
    }
    /**
     * Defines a switch nodes: Will break after first true condition
     * @param cases {...{ cond : IValue<boolean> | boolean, cb : function(RepeatNodeItem, ?number) }}
     * @return {BaseNode}
     */

  }, {
    key: "$defSwitch",
    value: function $defSwitch() {
      var $ = this.$;
      var default_ = $.slots.get("default");

      for (var _len9 = arguments.length, cases = new Array(_len9), _key9 = 0; _key9 < _len9; _key9++) {
        cases[_key9] = arguments[_key9];
      }

      if (default_ && default_ !== this && !$.building) {
        default_.$defSwitch.apply(default_, cases);
        return this;
      }

      var node = new SwitchedNode();
      node.$.parent = this;
      node.$$preinitShadow($.app, this.$.rt, this, null);
      node.$init();
      this.$$pushNode(node);
      node.setCases(cases);
      $.app.$run.callCallback(function () {
        node.$ready();
      });
      return this;
    }
  }]);

  return BaseNode;
}(_interfaces_core_js__WEBPACK_IMPORTED_MODULE_4__.VasilleNode);
/**
 * Represents an Vasille.js HTML element node
 */

var TagNode = /*#__PURE__*/function (_BaseNode) {
  _inherits(TagNode, _BaseNode);

  var _super5 = _createSuper(TagNode);

  function TagNode() {
    var _this5;

    _classCallCheck(this, TagNode);

    for (var _len10 = arguments.length, args = new Array(_len10), _key10 = 0; _key10 < _len10; _key10++) {
      args[_key10] = arguments[_key10];
    }

    _this5 = _super5.call.apply(_super5, [this].concat(args));
    _this5.$node = void 0;
    return _this5;
  }

  _createClass(TagNode, [{
    key: "$$preinitElementNode",
    value:
    /**
     * Constructs a element node
     * @param app {AppNode} the app node
     * @param rt {BaseNode} The root node
     * @param ts {BaseNode} The this node
     * @param before {VasilleNode} Node to insert before it
     * @param tagName {String} Name of HTML tag
     */
    function $$preinitElementNode(app, rt, ts, before, tagName) {
      this.$node = document.createElement(tagName);
      this.$$preinitNode(app, rt, ts, before, this.$node);
    }
    /**
     * Runs GC
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      _get(_getPrototypeOf(TagNode.prototype), "$destroy", this).call(this);

      this.$node.remove(); //$FlowFixMe

      this.$node = null;
    }
  }]);

  return TagNode;
}(BaseNode);
/**
 * Represents a Vasille.js shadow node
 */

var ExtensionNode = /*#__PURE__*/function (_BaseNode2) {
  _inherits(ExtensionNode, _BaseNode2);

  var _super6 = _createSuper(ExtensionNode);

  function ExtensionNode() {
    _classCallCheck(this, ExtensionNode);

    return _super6.apply(this, arguments);
  }

  _createClass(ExtensionNode, [{
    key: "$$preinitShadow",
    value:
    /**
     * Pre-initialize a shadow node
     * @param app {AppNode} the app node
     * @param rt {BaseNode} The root node
     * @param ts {BaseNode} The this node
     * @param before {VasilleNode} node to paste after it
     */
    function $$preinitShadow(app, rt, ts, before) {
      this.$.preinit(app, rt, ts, before);

      try {
        this.$.encapsulate(ts.$.el);
      } catch (e) {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.internalError)("A extension node can be encapsulated in a tag or extension node only");
      }
    }
    /**
     * Runs GC
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      _get(_getPrototypeOf(ExtensionNode.prototype), "$destroy", this).call(this);
    }
  }]);

  return ExtensionNode;
}(BaseNode);
/**
 * Defines a node which cas has just a child (TagNode | UserNode)
 */

var UserNode = /*#__PURE__*/function (_ExtensionNode) {
  _inherits(UserNode, _ExtensionNode);

  var _super7 = _createSuper(UserNode);

  function UserNode() {
    _classCallCheck(this, UserNode);

    return _super7.apply(this, arguments);
  }

  _createClass(UserNode, [{
    key: "$mounted",
    value: function $mounted() {
      _get(_getPrototypeOf(UserNode.prototype), "$mounted", this).call(this);

      if (this.$children.length !== 1) {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.userError)("UserNode must have a child only", "dom-error");
      }

      var child = this.$children[0];

      if (child instanceof TagNode || child instanceof UserNode) {
        var _this$$$el;

        this.$.encapsulate(child.$.el);
        (_this$$$el = this.$.el).vasille || (_this$$$el.vasille = this);
      } else {
        throw (0,_interfaces_errors_js__WEBPACK_IMPORTED_MODULE_5__.userError)("UserNode child must be TagNode or UserNode", "dom-error");
      }
    }
  }]);

  return UserNode;
}(ExtensionNode);

/**
 * Defines a abstract node, which represents a dynamical part of application
 */
var RepeatNodeItem = /*#__PURE__*/function (_ExtensionNode2) {
  _inherits(RepeatNodeItem, _ExtensionNode2);

  var _super8 = _createSuper(RepeatNodeItem);

  /**
   * node identifier
   * @type {*}
   */

  /**
   * Constructs a repeat node item
   * @param id {*}
   */
  function RepeatNodeItem(id) {
    var _this6;

    _classCallCheck(this, RepeatNodeItem);

    _this6 = _super8.call(this);
    _this6.$id = void 0;
    _this6.$id = id;
    return _this6;
  }
  /**
   * Destroy all children
   */


  _createClass(RepeatNodeItem, [{
    key: "$destroy",
    value: function $destroy() {
      this.$id = null;

      _get(_getPrototypeOf(RepeatNodeItem.prototype), "$destroy", this).call(this);
    }
  }]);

  return RepeatNodeItem;
}(ExtensionNode);
/**
 * Private part of switch node
 */

var SwitchedNodePrivate = /*#__PURE__*/function (_BaseNodePrivate) {
  _inherits(SwitchedNodePrivate, _BaseNodePrivate);

  var _super9 = _createSuper(SwitchedNodePrivate);

  function SwitchedNodePrivate() {
    var _this7;

    _classCallCheck(this, SwitchedNodePrivate);

    for (var _len11 = arguments.length, args = new Array(_len11), _key11 = 0; _key11 < _len11; _key11++) {
      args[_key11] = arguments[_key11];
    }

    _this7 = _super9.call.apply(_super9, [this].concat(args));
    _this7.index = -1;
    _this7.node = void 0;
    _this7.cases = void 0;
    _this7.sync = void 0;
    return _this7;
  }

  _createClass(SwitchedNodePrivate, [{
    key: "$destroy",
    value:
    /**
     * Runs GC
     */
    function $destroy() {
      //$FlowFixMe
      this.index = null; //$FlowFixMe

      this.node = null;

      var _iterator10 = _createForOfIteratorHelper(this.cases),
          _step10;

      try {
        for (_iterator10.s(); !(_step10 = _iterator10.n()).done;) {
          var c = _step10.value;
          //$FlowFixMe
          delete c.cond; //$FlowFixMe

          delete c.cb;
        }
      } catch (err) {
        _iterator10.e(err);
      } finally {
        _iterator10.f();
      }

      this.cases.splice(0); //$FlowFixMe

      this.cases = null; //$FlowFixMe

      this.sync = null;

      _get(_getPrototypeOf(SwitchedNodePrivate.prototype), "$destroy", this).call(this);
    }
  }]);

  return SwitchedNodePrivate;
}(BaseNodePrivate);
/**
 * Defines a node witch can switch its children conditionally
 */

var SwitchedNode = /*#__PURE__*/function (_ExtensionNode3) {
  _inherits(SwitchedNode, _ExtensionNode3);

  var _super10 = _createSuper(SwitchedNode);

  /**
   * Constructs a switch node and define a sync function
   */
  function SwitchedNode($) {
    var _this8;

    _classCallCheck(this, SwitchedNode);

    _this8 = _super10.call(this, $ || new SwitchedNodePrivate());

    _this8.$.sync = function () {
      var $ = _this8.$;
      var i = 0;

      for (; i < $.cases.length; i++) {
        if ($.cases[i].cond.$) {
          break;
        }
      }

      if (i === $.index) {
        return;
      }

      if ($.node) {
        $.node.$destroy();

        _this8.$children.splice(_this8.$children.indexOf($.node), 1); //$FlowFixMe


        $.node = null;
      }

      if (i !== $.cases.length) {
        $.index = i;

        _this8.createChild(i, $.cases[i].cb);
      } else {
        $.index = -1;
      }
    };

    return _this8;
  }

  _createClass(SwitchedNode, [{
    key: "setCases",
    value:
    /**
     * Set up switch cases
     * @param cases {{ cond : IValue | boolean, cb : function(RepeatNodeItem, ?number) }}
     */
    function setCases(cases) {
      var $ = this.$;
      $.cases = [];

      var _iterator11 = _createForOfIteratorHelper(cases),
          _step11;

      try {
        for (_iterator11.s(); !(_step11 = _iterator11.n()).done;) {
          var case_ = _step11.value;
          $.cases.push({
            cond: (0,_models_js__WEBPACK_IMPORTED_MODULE_9__.vassilify)(case_.cond),
            cb: case_.cb
          });
        }
      } catch (err) {
        _iterator11.e(err);
      } finally {
        _iterator11.f();
      }
    }
    /**
     * Creates a child node
     * @param id {*} id of node
     * @param cb {function(RepeatNodeItem, *)} Call-back
     */

  }, {
    key: "createChild",
    value: function createChild(id, cb) {
      var node = new RepeatNodeItem(id);
      node.$.parent = this;
      node.$$preinitShadow(this.$.app, this.$.rt, this);
      node.$init();
      cb(node, id);
      node.$ready();
      this.$.node = node;
      this.$children.push(node);
    }
  }, {
    key: "$$preinitShadow",
    value:
    /**
     * Prepare shadow node
     * @param app {AppNode} App node
     * @param rt {BaseNode} Root node
     * @param ts {BaseNode} This node
     * @param before {VasilleNode} The next node
     */
    function $$preinitShadow(app, rt, ts, before) {
      _get(_getPrototypeOf(SwitchedNode.prototype), "$$preinitShadow", this).call(this, app, rt, ts, before);

      this.$.encapsulate(ts.$.el);
    }
    /**
     * Run then the node is ready
     */

  }, {
    key: "$ready",
    value: function $ready() {
      var $ = this.$;

      _get(_getPrototypeOf(SwitchedNode.prototype), "$ready", this).call(this);

      var _iterator12 = _createForOfIteratorHelper($.cases),
          _step12;

      try {
        for (_iterator12.s(); !(_step12 = _iterator12.n()).done;) {
          var c = _step12.value;
          c.cond.on($.sync);
        }
      } catch (err) {
        _iterator12.e(err);
      } finally {
        _iterator12.f();
      }

      $.sync();
    }
    /**
     * Unbind and clear dynamical nodes
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      var $ = this.$;

      var _iterator13 = _createForOfIteratorHelper($.cases),
          _step13;

      try {
        for (_iterator13.s(); !(_step13 = _iterator13.n()).done;) {
          var c = _step13.value;
          c.cond.off($.sync);
        }
      } catch (err) {
        _iterator13.e(err);
      } finally {
        _iterator13.f();
      }

      _get(_getPrototypeOf(SwitchedNode.prototype), "$destroy", this).call(this);
    }
  }]);

  return SwitchedNode;
}(ExtensionNode);
/**
 * Represents a Vasille.js application node
 */


var AppNode = /*#__PURE__*/function (_BaseNode3) {
  _inherits(AppNode, _BaseNode3);

  var _super11 = _createSuper(AppNode);

  /**
   * The debug state of application, if true will output debug data
   * @type {boolean}
   */

  /**
   * Executor is use to optimize the page creation/update
   * @type {Executor}
   */

  /**
   * Constructs a app node
   * @param node {HTMLElement} The root of application
   * @param props {{debug : boolean}} Application properties
   */
  function AppNode(node, props) {
    var _this9;

    _classCallCheck(this, AppNode);

    _this9 = _super11.call(this);
    _this9.$debug = false;
    _this9.$run = void 0;
    _this9.$run = new _executor_js__WEBPACK_IMPORTED_MODULE_3__.InstantExecutor();

    _this9.$.encapsulate(node);

    _this9.$.preinit(_assertThisInitialized(_this9), _assertThisInitialized(_this9), _assertThisInitialized(_this9), _assertThisInitialized(_this9));

    if (props.debug instanceof Boolean) {
      _this9.$debug = props.debug;
    }

    return _this9;
  }

  _createClass(AppNode, [{
    key: "$destroy",
    value: function $destroy() {
      //$FlowFixMe
      this.$run = null;

      _get(_getPrototypeOf(AppNode.prototype), "$destroy", this).call(this);
    }
  }]);

  return AppNode;
}(BaseNode);

/***/ }),

/***/ "./node_modules/vasille-js/src/property.js":
/*!*************************************************!*\
  !*** ./node_modules/vasille-js/src/property.js ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "propertify": function() { return /* binding */ propertify; }
/* harmony export */ });
/* harmony import */ var _interfaces_idefinition_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./interfaces/idefinition.js */ "./node_modules/vasille-js/src/interfaces/idefinition.js");
/* harmony import */ var _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interfaces/ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
/* harmony import */ var _value_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./value.js */ "./node_modules/vasille-js/src/value.js");



/**
 * Constructs a property field value
 * @param value {?any} is the initial value of field
 * @param func {?Callable} is the function to calc filed value
 * @return {IValue} Given value or new generated
 */

function propertify() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var func = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

  if (func) {
    var v = func.func();

    if (v instanceof _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_1__.IValue) {
      return v;
    } else {
      return new _value_js__WEBPACK_IMPORTED_MODULE_2__.Reference(v);
    }
  } else {
    if (value instanceof _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_1__.IValue) {
      return value;
    } else {
      return new _value_js__WEBPACK_IMPORTED_MODULE_2__.Reference(value);
    }
  }
}

/***/ }),

/***/ "./node_modules/vasille-js/src/style.js":
/*!**********************************************!*\
  !*** ./node_modules/vasille-js/src/style.js ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "stylify": function() { return /* binding */ stylify; },
/* harmony export */   "StyleBinding": function() { return /* binding */ StyleBinding; }
/* harmony export */ });
/* harmony import */ var _bind_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bind.js */ "./node_modules/vasille-js/src/bind.js");
/* harmony import */ var _interfaces_idefinition_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interfaces/idefinition.js */ "./node_modules/vasille-js/src/interfaces/idefinition.js");
/* harmony import */ var _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./interfaces/ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
/* harmony import */ var _property_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./property.js */ "./node_modules/vasille-js/src/property.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }





/**
 * Constructs a style attribute value
 * @param rt {BaseNode} The root node
 * @param ts {BaseNode} The this node
 * @param name {String} The style attribute name
 * @param value {String | IValue | null} A value for attribute
 * @param func {?Callable} A getter of attribute value
 * @return {StyleBinding} A ready style binding
 */

function stylify(rt, ts, name) {
  var value = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  var func = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
  return new StyleBinding(rt, ts, name, null, (0,_property_js__WEBPACK_IMPORTED_MODULE_3__.propertify)(value, func));
}
/**
 * Describes a style attribute binding
 * @extends Binding
 */

var StyleBinding = /*#__PURE__*/function (_Binding) {
  _inherits(StyleBinding, _Binding);

  var _super = _createSuper(StyleBinding);

  /**
   * Constructs a style binding attribute
   * @param rt {BaseNode} is root component
   * @param ts {BaseNode} is this component
   * @param name {string} is the name of style property
   * @param func {function} is the function to calc style value
   * @param values is the value to be synced
   */
  function StyleBinding(rt, ts, name, func) {
    _classCallCheck(this, StyleBinding);

    for (var _len = arguments.length, values = new Array(_len > 4 ? _len - 4 : 0), _key = 4; _key < _len; _key++) {
      values[_key - 4] = arguments[_key];
    }

    return _super.call.apply(_super, [this, rt, ts, name, func].concat(values));
  }
  /**
   * Generates a function to update style property value
   * @param name {string}
   * @returns {Function} a function to update style property
   */


  _createClass(StyleBinding, [{
    key: "bound",
    value: function bound(name) {
      return function (rt, ts, value) {
        if (rt.$) {
          rt.$.app.$run.setStyle(ts.$.el, name, value);
        }

        return value;
      };
    }
  }]);

  return StyleBinding;
}(_bind_js__WEBPACK_IMPORTED_MODULE_0__.Binding);

/***/ }),

/***/ "./node_modules/vasille-js/src/value.js":
/*!**********************************************!*\
  !*** ./node_modules/vasille-js/src/value.js ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Reference": function() { return /* binding */ Reference; },
/* harmony export */   "Pointer": function() { return /* binding */ Pointer; }
/* harmony export */ });
/* harmony import */ var _interfaces_errors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./interfaces/errors */ "./node_modules/vasille-js/src/interfaces/errors.js");
/* harmony import */ var _interfaces_idefinition__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interfaces/idefinition */ "./node_modules/vasille-js/src/interfaces/idefinition.js");
/* harmony import */ var _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./interfaces/ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }




/**
 * Declares a notifiable value
 * @implements IValue
 */

var Reference = /*#__PURE__*/function (_IValue) {
  _inherits(Reference, _IValue);

  var _super = _createSuper(Reference);

  /**
   * The encapsulated value
   * @type {*}
   */

  /**
   * Array of handlers
   * @type {Set<Function>}
   */

  /**
   * Constructs a notifiable value
   * @param value {any} is initial value
   */
  function Reference(value) {
    var _this;

    _classCallCheck(this, Reference);

    _this = _super.call(this);
    _this.value = void 0;
    _this.onchange = void 0;
    _this.value = value;
    _this.onchange = new Set();
    return _this;
  }
  /**
   * Gets the notifiable value as js value
   * @returns {any} contained value
   */


  _createClass(Reference, [{
    key: "$",
    get: function get() {
      return this.value;
    }
    /**
     * Sets the value and notify listeners
     * @param value {any} is the new value
     * @returns {Reference} a pointer to this
     */
    ,
    set: function set(value) {
      if (this.value !== value) {
        if (this.type && !(0,_interfaces_idefinition__WEBPACK_IMPORTED_MODULE_1__.checkType)(value, this.type)) {
          throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.typeError)("Unable to set reference value");
        }

        this.value = value;

        var _iterator = _createForOfIteratorHelper(this.onchange),
            _step;

        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var handler = _step.value;
            handler(value);
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }

      return this;
    }
    /**
     * Adds a new handler for value change
     * @param handler {function} is a user-defined event handler
     * @returns {Reference} a pointer to this
     */

  }, {
    key: "on",
    value: function on(handler) {
      this.onchange.add(handler);
      return this;
    }
    /**
     * Removes a new handler for value change
     * @param handler {function} is a existing user-defined handler
     * @returns {Reference} a pointer to this
     */

  }, {
    key: "off",
    value: function off(handler) {
      this.onchange["delete"](handler);
      return this;
    }
    /**
     * Runs GC
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      _get(_getPrototypeOf(Reference.prototype), "$destroy", this).call(this);

      this.onchange.clear();
    }
  }]);

  return Reference;
}(_interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_2__.IValue);
/**
 * Declares a notifiable bind to a value
 * @extends IValue
 */

var Pointer = /*#__PURE__*/function (_IValue2) {
  _inherits(Pointer, _IValue2);

  var _super2 = _createSuper(Pointer);

  /**
   * value of pointer
   * @type {IValue<*>}
   */

  /**
   * Collection of handlers
   * @type {Set<Function>}
   */

  /**
   * Constructs a notifiable bind to a value
   * @param value {IValue} is initial value
   */
  function Pointer(value) {
    var _this2;

    _classCallCheck(this, Pointer);

    _this2 = _super2.call(this);
    _this2.value = void 0;
    _this2.onchange = void 0;
    _this2.onchange = new Set();
    _this2.$ = value;
    return _this2;
  }
  /**
   * Gets the notifiable value as js value
   * @returns {any} contained value
   */


  _createClass(Pointer, [{
    key: "$",
    get: function get() {
      return this.value.$;
    }
    /**
     * Sets the value and notify listeners
     * @param value {IValue} is the new value
     * @returns {IValue} a pointer to this
     */
    ,
    set: function set(value) {
      if (this.value !== value) {
        if (this.type) {
          if (this.type !== value.type) {
            if (value.type === null && (0,_interfaces_idefinition__WEBPACK_IMPORTED_MODULE_1__.checkType)(value.$, this.type)) {
              value.type = this.type;
            } else if (!(0,_interfaces_idefinition__WEBPACK_IMPORTED_MODULE_1__.isSubclassOf)(this.type, value.type)) {
              throw (0,_interfaces_errors__WEBPACK_IMPORTED_MODULE_0__.typeError)("reference type incompatible with pointer type");
            }
          }
        }

        var _iterator2 = _createForOfIteratorHelper(this.onchange),
            _step2;

        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var _handler = _step2.value;
            this.value.off(_handler);
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }

        this.value = value;

        var _iterator3 = _createForOfIteratorHelper(this.onchange),
            _step3;

        try {
          for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
            var _handler2 = _step3.value;
            this.value.on(_handler2);
          }
        } catch (err) {
          _iterator3.e(err);
        } finally {
          _iterator3.f();
        }

        if (this.value.$ !== value.$) {
          var _iterator4 = _createForOfIteratorHelper(this.onchange),
              _step4;

          try {
            for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
              var handler = _step4.value;
              handler(this.value.$);
            }
          } catch (err) {
            _iterator4.e(err);
          } finally {
            _iterator4.f();
          }
        }
      }

      return this;
    }
    /**
     * Adds a new handler for value change
     * @param handler {function} is a user-defined event handler
     * @returns {IValue} a pointer to this
     */

  }, {
    key: "on",
    value: function on(handler) {
      this.onchange.add(handler);
      this.value.on(handler);
      return this;
    }
    /**
     * Removes a new handler for value change
     * @param handler {function} is a existing user-defined handler
     * @returns {IValue} a pointer to this
     */

  }, {
    key: "off",
    value: function off(handler) {
      if (this.onchange.has(handler)) {
        this.value.off(handler);
        this.onchange["delete"](handler);
      }

      return this;
    }
    /**
     * Removes all bounded functions
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      var _iterator5 = _createForOfIteratorHelper(this.onchange),
          _step5;

      try {
        for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
          var handler = _step5.value;
          this.value.off(handler);
        }
      } catch (err) {
        _iterator5.e(err);
      } finally {
        _iterator5.f();
      }
    }
  }]);

  return Pointer;
}(_interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_2__.IValue);

/***/ }),

/***/ "./node_modules/vasille-js/src/views.js":
/*!**********************************************!*\
  !*** ./node_modules/vasille-js/src/views.js ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RepeatNodePrivate": function() { return /* binding */ RepeatNodePrivate; },
/* harmony export */   "RepeatNode": function() { return /* binding */ RepeatNode; },
/* harmony export */   "RepeaterPrivate": function() { return /* binding */ RepeaterPrivate; },
/* harmony export */   "Repeater": function() { return /* binding */ Repeater; },
/* harmony export */   "BaseViewPrivate": function() { return /* binding */ BaseViewPrivate; },
/* harmony export */   "BaseView": function() { return /* binding */ BaseView; },
/* harmony export */   "ArrayViewPrivate": function() { return /* binding */ ArrayViewPrivate; },
/* harmony export */   "ArrayView": function() { return /* binding */ ArrayView; },
/* harmony export */   "ObjectView": function() { return /* binding */ ObjectView; },
/* harmony export */   "MapView": function() { return /* binding */ MapView; },
/* harmony export */   "SetView": function() { return /* binding */ SetView; }
/* harmony export */ });
/* harmony import */ var _interfaces_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./interfaces/core */ "./node_modules/vasille-js/src/interfaces/core.js");
/* harmony import */ var _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interfaces/ivalue.js */ "./node_modules/vasille-js/src/interfaces/ivalue.js");
/* harmony import */ var _models_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./models.js */ "./node_modules/vasille-js/src/models.js");
/* harmony import */ var _node_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node.js */ "./node_modules/vasille-js/src/node.js");
/* harmony import */ var _value_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./value.js */ "./node_modules/vasille-js/src/value.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }






var RepeatNodePrivate = /*#__PURE__*/function (_BaseNodePrivate) {
  _inherits(RepeatNodePrivate, _BaseNodePrivate);

  var _super = _createSuper(RepeatNodePrivate);

  function RepeatNodePrivate() {
    var _this;

    _classCallCheck(this, RepeatNodePrivate);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.nodes = new Map();
    _this.cb = void 0;
    return _this;
  }

  return RepeatNodePrivate;
}(_node_js__WEBPACK_IMPORTED_MODULE_3__.BaseNodePrivate);
/**
 * Repeat node repeats its children
 */

var RepeatNode = /*#__PURE__*/function (_ExtensionNode) {
  _inherits(RepeatNode, _ExtensionNode);

  var _super2 = _createSuper(RepeatNode);

  function RepeatNode($) {
    _classCallCheck(this, RepeatNode);

    return _super2.call(this, $ || new RepeatNodePrivate());
  }
  /**
   * Sets call-back function
   * @param cb {function(RepeatNodeItem, ?*) : void}
   */


  _createClass(RepeatNode, [{
    key: "setCallback",
    value: function setCallback(cb) {
      this.$.cb = cb;
    }
    /**
     * Initialize the shadow node
     * @param app {AppNode} App node
     * @param rt {BaseNode} Root node
     * @param ts {BaseNode} This node
     * @param before {VasilleNode} Node to paste content before it
     */

  }, {
    key: "$$preinitShadow",
    value: function $$preinitShadow(app, rt, ts, before) {
      var $ = this.$;

      _get(_getPrototypeOf(RepeatNode.prototype), "$$preinitShadow", this).call(this, app, rt, ts, before);

      $.encapsulate($.el);
    }
    /**
     * Create a children pack
     * @param id {*} id of child pack
     * @param item {IValue<*>} value for children pack
     * @param before {VasilleNode} Node to insert before it
     */

  }, {
    key: "createChild",
    value: function createChild(id, item, before) {
      var _this2 = this;

      var current = this.$.nodes.get(id);
      var node = new _node_js__WEBPACK_IMPORTED_MODULE_3__.RepeatNodeItem(id);
      var $ = node.$;
      this.destroyChild(id, item);
      $.parent = this;

      if (current) {
        $.next = current.$.next;
        $.prev = current.$.prev;

        if ($.next) {
          $.next.$.prev = node;
        }

        if ($.prev) {
          $.prev.$.next = node;
        }

        this.$children.splice(this.$children.indexOf(current), 1, node);
      } else if (before) {
        $.next = before;
        $.prev = before.$.prev;
        before.$.prev = node;

        if ($.prev) {
          $.prev.$.next = node;
        }

        this.$children.splice(this.$children.indexOf(before) - 1, 0, node);
      } else {
        var lastChild = this.$children[this.$children.length - 1];

        if (lastChild) {
          lastChild.$.next = node;
        }

        $.prev = lastChild;
        this.$children.push(node);
      }

      node.$$preinitShadow(this.$.app, this.$.rt, this, before || (current ? current.$.next : null));
      node.$init();
      this.$.app.$run.callCallback(function () {
        _this2.$.cb(node, item.$);

        node.$ready();
      });
      this.$.nodes.set(id, node);
    }
  }, {
    key: "destroyChild",
    value:
    /**
     * Destroy a old child
     * @param id {*} id of children pack
     * @param item {IValue<*>} value of children pack
     */
    function destroyChild(id, item) {
      var child = this.$.nodes.get(id);

      if (child) {
        var $ = child.$;

        if ($.prev) {
          $.prev.$.next = $.next;
        }

        if ($.next) {
          $.next.$.prev = $.prev;
        }

        child.$destroy();
        this.$.nodes["delete"](id);
        this.$children.splice(this.$children.indexOf(child), 1);
      }
    }
  }]);

  return RepeatNode;
}(_node_js__WEBPACK_IMPORTED_MODULE_3__.ExtensionNode);
/**
 * Private part of repeater
 */

var RepeaterPrivate = /*#__PURE__*/function (_RepeatNodePrivate) {
  _inherits(RepeaterPrivate, _RepeatNodePrivate);

  var _super3 = _createSuper(RepeaterPrivate);

  function RepeaterPrivate() {
    var _this3;

    _classCallCheck(this, RepeaterPrivate);

    for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    _this3 = _super3.call.apply(_super3, [this].concat(args));
    _this3.updateHandler = void 0;
    _this3.currentCount = 0;
    _this3.orderNumber = [];
    return _this3;
  }

  return RepeaterPrivate;
}(RepeatNodePrivate);
/**
 * The simplest repeat $node interpretation, repeat children pack a several times
 */

var Repeater = /*#__PURE__*/function (_RepeatNode) {
  _inherits(Repeater, _RepeatNode);

  var _super4 = _createSuper(Repeater);

  /**
   * The count of children
   * @type {IValue<number>}
   */
  function Repeater($) {
    var _this4;

    _classCallCheck(this, Repeater);

    _this4 = _super4.call(this, $ || new RepeaterPrivate());
    _this4.count = new _value_js__WEBPACK_IMPORTED_MODULE_4__.Reference(0);
    return _this4;
  }
  /**
   * Changes the children count
   * @param number {number} The new children count
   */


  _createClass(Repeater, [{
    key: "changeCount",
    value: function changeCount(number) {
      var $ = this.$;

      if (number > $.currentCount) {
        for (var i = $.currentCount; i < number; i++) {
          var item = new _value_js__WEBPACK_IMPORTED_MODULE_4__.Reference(i);
          this.createChild(i, item);
          $.orderNumber.push(item);
        }
      } else {
        for (var _i = $.currentCount - 1; _i >= number; _i--) {
          this.destroyChild(_i, $.orderNumber[_i]);
        }

        $.orderNumber.splice(number);
      }

      $.currentCount = number;
    }
    /**
     * Handles created event
     */

  }, {
    key: "$created",
    value: function $created() {
      var _this5 = this;

      var $ = this.$;

      _get(_getPrototypeOf(Repeater.prototype), "$created", this).call(this);

      $.updateHandler = function (value) {
        _this5.changeCount(value);
      };

      this.count.on($.updateHandler);
    }
    /**
     * Handles ready event
     */

  }, {
    key: "$ready",
    value: function $ready() {
      this.changeCount(this.count.$);
    }
    /**
     * Handles destroy event
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      var $ = this.$;

      _get(_getPrototypeOf(Repeater.prototype), "$destroy", this).call(this);

      this.count.off($.updateHandler);
    }
  }]);

  return Repeater;
}(RepeatNode);
/**
 * Private part of BaseView
 */

var BaseViewPrivate = /*#__PURE__*/function (_RepeatNodePrivate2) {
  _inherits(BaseViewPrivate, _RepeatNodePrivate2);

  var _super5 = _createSuper(BaseViewPrivate);

  function BaseViewPrivate() {
    var _this6;

    _classCallCheck(this, BaseViewPrivate);

    for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      args[_key3] = arguments[_key3];
    }

    _this6 = _super5.call.apply(_super5, [this].concat(args));
    _this6.addHandler = void 0;
    _this6.removeHandler = void 0;
    return _this6;
  }

  return BaseViewPrivate;
}(RepeatNodePrivate);
/**
 * Base class of default views
 */

var BaseView = /*#__PURE__*/function (_RepeatNode2) {
  _inherits(BaseView, _RepeatNode2);

  var _super6 = _createSuper(BaseView);

  // props

  /**
   * Property which will contain a model
   * @type {IValue<*>}
   */

  /**
   * Sets up model and handlers
   */
  function BaseView($1) {
    var _this7;

    _classCallCheck(this, BaseView);

    _this7 = _super6.call(this, $1 || new BaseViewPrivate());
    _this7.model = void 0;
    var $ = _this7.$;

    $.addHandler = function (id, item) {
      _this7.createChild(id, item);
    };

    $.removeHandler = function (id, item) {
      _this7.destroyChild(id, item);
    };

    return _this7;
  }
  /**
   * Creates a child when user adds new values
   * @param id {*} id of children pack
   * @param item {IValue<*>} Reference of children pack
   * @param before {VasilleNode} Node to paste before it
   * @return {handler} handler must be saved and unliked on value remove
   */


  _createClass(BaseView, [{
    key: "createChild",
    value: function createChild(id, item, before) {
      var _this8 = this;

      var handler = function handler() {
        _this8.createChild(id, item);
      };

      item.on(handler);

      _get(_getPrototypeOf(BaseView.prototype), "createChild", this).call(this, id, item, before);

      return handler;
    }
    /**
     * Handle ready event
     */

  }, {
    key: "$ready",
    value: function $ready() {
      var $ = this.$;
      this.model.$.listener.onAdd($.addHandler);
      this.model.$.listener.onRemove($.removeHandler);

      _get(_getPrototypeOf(BaseView.prototype), "$ready", this).call(this);
    }
    /**
     * Handles destroy event
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      var $ = this.$;
      this.model.$.listener.offAdd($.addHandler);
      this.model.$.listener.offRemove($.removeHandler);

      _get(_getPrototypeOf(BaseView.prototype), "$destroy", this).call(this);
    }
  }]);

  return BaseView;
}(RepeatNode);
/**
 * Private part of array view
 */

var ArrayViewPrivate = /*#__PURE__*/function (_BaseViewPrivate) {
  _inherits(ArrayViewPrivate, _BaseViewPrivate);

  var _super7 = _createSuper(ArrayViewPrivate);

  function ArrayViewPrivate() {
    var _this9;

    _classCallCheck(this, ArrayViewPrivate);

    for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
      args[_key4] = arguments[_key4];
    }

    _this9 = _super7.call.apply(_super7, [this].concat(args));
    _this9.handlers = new Map();
    _this9.buffer = [];
    return _this9;
  }

  return ArrayViewPrivate;
}(BaseViewPrivate);
/**
 * Represents a view of a array model
 */

var ArrayView = /*#__PURE__*/function (_BaseView) {
  _inherits(ArrayView, _BaseView);

  var _super8 = _createSuper(ArrayView);

  /**
   * model of view
   * @type {IValue<ArrayModel<*>>}
   */

  /**
   * Sets up model with a default value
   */
  function ArrayView($) {
    var _this10;

    _classCallCheck(this, ArrayView);

    _this10 = _super8.call(this, $ || new ArrayViewPrivate());
    _this10.model = void 0;
    _this10.model = _this10.$public(_models_js__WEBPACK_IMPORTED_MODULE_2__.ArrayModel);
    return _this10;
  }
  /**
   * Overrides child created and generate random id for children
   */


  _createClass(ArrayView, [{
    key: "createChild",
    value: function createChild(id, item, before) {
      var $ = this.$;
      var next = typeof id === "number" ? $.nodes.get($.buffer[id]) : null;

      var handler = _get(_getPrototypeOf(ArrayView.prototype), "createChild", this).call(this, item, item, before || next);

      $.handlers.set(item, handler);
      $.buffer.splice(id, 0, item);
    }
    /**
     * Removes a children pack
     */

  }, {
    key: "destroyChild",
    value: function destroyChild(id, item) {
      var $ = this.$;
      var index = typeof id === "number" ? id : $.buffer.indexOf(item);

      if (index === -1) {
        return;
      }

      item.off($.handlers.get(item));
      $.handlers["delete"](item);

      _get(_getPrototypeOf(ArrayView.prototype), "destroyChild", this).call(this, item, item);

      $.buffer.splice(index, 1);
    }
    /**
     * Handle ready event
     */

  }, {
    key: "$ready",
    value: function $ready() {
      var _this11 = this;

      var $ = this.$;
      var arr = this.model.$;

      var _loop = function _loop(i) {
        $.app.$run.callCallback(function () {
          _this11.createChild(i, arr[i]);
        });
      };

      for (var i = 0; i < arr.length; i++) {
        _loop(i);
      }

      _get(_getPrototypeOf(ArrayView.prototype), "$ready", this).call(this);
    }
    /**
     * Handle destroy event
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      var _iterator = _createForOfIteratorHelper(this.$.handlers),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var it = _step.value;
          it[0].off(it[1]);
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      _get(_getPrototypeOf(ArrayView.prototype), "$destroy", this).call(this);
    }
  }]);

  return ArrayView;
}(BaseView);
/**
 * private part of object view
 */

var ObjectViewPrivate = /*#__PURE__*/function (_BaseViewPrivate2) {
  _inherits(ObjectViewPrivate, _BaseViewPrivate2);

  var _super9 = _createSuper(ObjectViewPrivate);

  function ObjectViewPrivate() {
    var _this12;

    _classCallCheck(this, ObjectViewPrivate);

    for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
      args[_key5] = arguments[_key5];
    }

    _this12 = _super9.call.apply(_super9, [this].concat(args));
    _this12.handlers = {};
    return _this12;
  }

  return ObjectViewPrivate;
}(BaseViewPrivate);
/**
 * Create a children pack for each object field
 */


var ObjectView = /*#__PURE__*/function (_BaseView2) {
  _inherits(ObjectView, _BaseView2);

  var _super10 = _createSuper(ObjectView);

  /**
   * Sets up model
   */
  function ObjectView($) {
    var _this13;

    _classCallCheck(this, ObjectView);

    _this13 = _super10.call(this, $ || new ObjectViewPrivate());
    _this13.model = _this13.$public(_models_js__WEBPACK_IMPORTED_MODULE_2__.ObjectModel);
    return _this13;
  }
  /**
   * Saves the child handler
   */


  _createClass(ObjectView, [{
    key: "createChild",
    value: function createChild(id, item, before) {
      this.$.handlers[id] = _get(_getPrototypeOf(ObjectView.prototype), "createChild", this).call(this, id, item, before);
    }
    /**
     * Disconnects the child handler
     */

  }, {
    key: "destroyChild",
    value: function destroyChild(id, item) {
      var $ = this.$;
      item.off($.handlers[id]);
      delete $.handlers[id];

      _get(_getPrototypeOf(ObjectView.prototype), "destroyChild", this).call(this, id, item);
    }
    /**
     * Handler ready event
     */

  }, {
    key: "$ready",
    value: function $ready() {
      var _this14 = this;

      var $ = this.$;
      var obj = this.model.$;

      var _loop2 = function _loop2(i) {
        if (obj.hasOwnProperty(i) && obj.get(i) instanceof _interfaces_ivalue_js__WEBPACK_IMPORTED_MODULE_1__.IValue) {
          $.app.$run.callCallback(function () {
            _this14.createChild(i, obj.get(i));
          });
        }
      };

      for (var i in obj) {
        _loop2(i);
      }

      _get(_getPrototypeOf(ObjectView.prototype), "$ready", this).call(this);
    }
    /**
     * Handler destroy event
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      var $ = this.$;
      var obj = this.model.$;

      for (var i in obj) {
        if (obj.hasOwnProperty(i)) {
          obj.get(i).off($.handlers[i]);
        }
      }

      _get(_getPrototypeOf(ObjectView.prototype), "$destroy", this).call(this);
    }
  }]);

  return ObjectView;
}(BaseView);
/**
 * private part of map view
 */

var MapViewPrivate = /*#__PURE__*/function (_BaseViewPrivate3) {
  _inherits(MapViewPrivate, _BaseViewPrivate3);

  var _super11 = _createSuper(MapViewPrivate);

  function MapViewPrivate() {
    var _this15;

    _classCallCheck(this, MapViewPrivate);

    for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
      args[_key6] = arguments[_key6];
    }

    _this15 = _super11.call.apply(_super11, [this].concat(args));
    _this15.handlers = new Map();
    return _this15;
  }

  return MapViewPrivate;
}(BaseViewPrivate);
/**
 * Create a children pack for each map value
 */


var MapView = /*#__PURE__*/function (_BaseView3) {
  _inherits(MapView, _BaseView3);

  var _super12 = _createSuper(MapView);

  /**
   * Sets up model
   */
  function MapView($) {
    var _this16;

    _classCallCheck(this, MapView);

    _this16 = _super12.call(this, $ || new MapViewPrivate());
    _this16.model = _this16.$public(_models_js__WEBPACK_IMPORTED_MODULE_2__.MapModel);
    return _this16;
  }
  /**
   * Saves the child handler
   */


  _createClass(MapView, [{
    key: "createChild",
    value: function createChild(id, item, before) {
      this.$.handlers.set(id, _get(_getPrototypeOf(MapView.prototype), "createChild", this).call(this, id, item, before));
    }
    /**
     * Disconnects the child handler
     */

  }, {
    key: "destroyChild",
    value: function destroyChild(id, item) {
      var $ = this.$;
      item.off($.handlers.get(id));
      $.handlers["delete"](id);

      _get(_getPrototypeOf(MapView.prototype), "destroyChild", this).call(this, id, item);
    }
    /**
     * Handler ready event
     */

  }, {
    key: "$ready",
    value: function $ready() {
      var _this17 = this;

      var $ = this.$;
      var map = this.model.$;

      var _iterator2 = _createForOfIteratorHelper(map),
          _step2;

      try {
        var _loop3 = function _loop3() {
          var it = _step2.value;
          $.app.$run.callCallback(function () {
            _this17.createChild(it[0], it[1]);
          });
        };

        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          _loop3();
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }

      _get(_getPrototypeOf(MapView.prototype), "$ready", this).call(this);
    }
    /**
     * Handler destroy event
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      var $ = this.$;
      var map = this.model.$;

      var _iterator3 = _createForOfIteratorHelper(map),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var it = _step3.value;
          it[1].off($.handlers.get(it[0]));
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }

      _get(_getPrototypeOf(MapView.prototype), "$destroy", this).call(this);
    }
  }]);

  return MapView;
}(BaseView);
/**
 * private part of set view
 */

var SetViewPrivate = /*#__PURE__*/function (_BaseViewPrivate4) {
  _inherits(SetViewPrivate, _BaseViewPrivate4);

  var _super13 = _createSuper(SetViewPrivate);

  function SetViewPrivate() {
    var _this18;

    _classCallCheck(this, SetViewPrivate);

    for (var _len7 = arguments.length, args = new Array(_len7), _key7 = 0; _key7 < _len7; _key7++) {
      args[_key7] = arguments[_key7];
    }

    _this18 = _super13.call.apply(_super13, [this].concat(args));
    _this18.handlers = new Map();
    return _this18;
  }

  return SetViewPrivate;
}(BaseViewPrivate);
/**
 * Create a children pack for each set value
 */


var SetView = /*#__PURE__*/function (_BaseView4) {
  _inherits(SetView, _BaseView4);

  var _super14 = _createSuper(SetView);

  /**
   * Sets up model
   */
  function SetView($) {
    var _this19;

    _classCallCheck(this, SetView);

    _this19 = _super14.call(this, $ || new SetViewPrivate());
    _this19.model = _this19.$public(_models_js__WEBPACK_IMPORTED_MODULE_2__.SetModel);
    return _this19;
  }
  /**
   * Saves the child handler
   */


  _createClass(SetView, [{
    key: "createChild",
    value: function createChild(id, item, before) {
      this.$.handlers.set(item, _get(_getPrototypeOf(SetView.prototype), "createChild", this).call(this, id, item, before));
    }
    /**
     * Disconnects the child handler
     */

  }, {
    key: "destroyChild",
    value: function destroyChild(id, item) {
      var $ = this.$;
      item.off($.handlers.get(item));
      $.handlers["delete"](item);

      _get(_getPrototypeOf(SetView.prototype), "destroyChild", this).call(this, id, item);
    }
    /**
     * Handler ready event
     */

  }, {
    key: "$ready",
    value: function $ready() {
      var _this20 = this;

      var $ = this.$;
      var set = this.model.$;

      var _iterator4 = _createForOfIteratorHelper(set),
          _step4;

      try {
        var _loop4 = function _loop4() {
          var it = _step4.value;
          $.app.$run.callCallback(function () {
            _this20.createChild(it, it);
          });
        };

        for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
          _loop4();
        }
      } catch (err) {
        _iterator4.e(err);
      } finally {
        _iterator4.f();
      }

      _get(_getPrototypeOf(SetView.prototype), "$ready", this).call(this);
    }
    /**
     * Handler destroy event
     */

  }, {
    key: "$destroy",
    value: function $destroy() {
      var $ = this.$;
      var set = this.model.$;

      var _iterator5 = _createForOfIteratorHelper(set),
          _step5;

      try {
        for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
          var it = _step5.value;
          it.off($.handlers.get(it));
        }
      } catch (err) {
        _iterator5.e(err);
      } finally {
        _iterator5.f();
      }

      _get(_getPrototypeOf(SetView.prototype), "$destroy", this).call(this);
    }
  }]);

  return SetView;
}(BaseView);

/***/ }),

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "App": function() { return /* binding */ App; }
/* harmony export */ });
/* harmony import */ var _components_x1_MapX1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/x1/MapX1 */ "./src/components/x1/MapX1.js");
/* harmony import */ var _types_block__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./types/block */ "./src/types/block.js");
/* harmony import */ var vasille_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vasille-js */ "./node_modules/vasille-js/src/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }




var s = 40;
var App = /*#__PURE__*/function (_AppNode) {
  _inherits(App, _AppNode);

  var _super = _createSuper(App);

  function App() {
    var _this;

    _classCallCheck(this, App);

    _this = _super.call(this, document.querySelector("body"), {
      debug: true
    });
    _this.blocks = void 0;
    _this.blocks = new _types_block__WEBPACK_IMPORTED_MODULE_1__.BlockPack(0, 0, 26 * s, 26 * s);
    return _this;
  }

  _createClass(App, [{
    key: "createLevel",
    value: function createLevel(data) {
      for (var i in data) {
        for (var j in data[i]) {
          var v = data[i][j];

          if (v !== 0) {
            if (v === 1) {
              this.blocks.els.add(new _types_block__WEBPACK_IMPORTED_MODULE_1__.BrickBlock(j * s, i * s));
            } else if (v === 2) {
              this.blocks.els.add(new _types_block__WEBPACK_IMPORTED_MODULE_1__.BetonBlock(j * s, i * s));
            }
          }
        }
      }

      var fastFind = {
        0: {},
        1: {},
        2: {},
        3: {}
      };
      var ls = 8 * s;

      for (var x = 0; x < 4; x++) {
        for (var y = 0; y < 4; y++) {
          var blockPack = new _types_block__WEBPACK_IMPORTED_MODULE_1__.BlockPack(x * ls, y * ls, ls, ls);
          this.blocks.sub.add(blockPack);
          fastFind[x][y] = blockPack;
        }
      }

      var _iterator = _createForOfIteratorHelper(this.blocks.els),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var block2 = _step.value;
          var block = block2.$;
          fastFind[Math.floor(block.x.$ / ls)][Math.floor(block.y.$ / ls)].els.add(block);
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      var _iterator2 = _createForOfIteratorHelper(this.blocks.sub),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var sub = _step2.value;

          if (sub.$.els.size === 0) {
            this.blocks.sub["delete"](sub);
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }

      ls = 2 * s;

      var _iterator3 = _createForOfIteratorHelper(this.blocks.sub),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var pack2 = _step3.value;
          var pack = pack2.$;

          for (var _x = 0; _x < 4; _x++) {
            for (var _y = 0; _y < 4; _y++) {
              var _blockPack = new _types_block__WEBPACK_IMPORTED_MODULE_1__.BlockPack(pack.x.$ + _x * ls, pack.y.$ + _y * ls, ls, ls);

              pack.sub.add(_blockPack);
              fastFind[_x][_y] = _blockPack;
            }
          }

          var _iterator4 = _createForOfIteratorHelper(pack.els),
              _step4;

          try {
            for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
              var _block = _step4.value;
              var _block2 = _block.$;
              fastFind[Math.floor((_block2.x.$ - pack.x.$) / ls)][Math.floor((_block2.y.$ - pack.y.$) / ls)].els.add(_block2);
            }
          } catch (err) {
            _iterator4.e(err);
          } finally {
            _iterator4.f();
          }

          var _iterator5 = _createForOfIteratorHelper(pack.sub),
              _step5;

          try {
            for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
              var _sub = _step5.value;

              if (_sub.$.els.size === 0) {
                pack.sub["delete"](_sub);
              }
            }
          } catch (err) {
            _iterator5.e(err);
          } finally {
            _iterator5.f();
          }
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }
    }
  }, {
    key: "$createDom",
    value: function $createDom() {
      var _this2 = this;

      _get(_getPrototypeOf(App.prototype), "$createDom", this).call(this);

      this.$defElement(new _components_x1_MapX1__WEBPACK_IMPORTED_MODULE_0__.MapX1(), function ($) {
        $.width = 1040;
        $.height = 1040;
        $.els = _this2.blocks.els;
        $.sub = _this2.blocks.sub;
      });
    }
  }]);

  return App;
}(vasille_js__WEBPACK_IMPORTED_MODULE_2__.AppNode);

/***/ }),

/***/ "./src/components/BaseBrick.js":
/*!*************************************!*\
  !*** ./src/components/BaseBrick.js ***!
  \*************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BaseBrick": function() { return /* binding */ BaseBrick; }
/* harmony export */ });
/* harmony import */ var vasille_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vasille-js */ "./node_modules/vasille-js/src/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }


var BaseBrick = /*#__PURE__*/function (_ExtensionNode) {
  _inherits(BaseBrick, _ExtensionNode);

  var _super = _createSuper(BaseBrick);

  function BaseBrick() {
    var _this;

    _classCallCheck(this, BaseBrick);

    _this = _super.call(this);
    _this.x = void 0;
    _this.y = void 0;
    _this.tx = void 0;
    _this.ty = void 0;
    _this.l1x = void 0;
    _this.l1y = void 0;
    _this.l2x = void 0;
    _this.l2y = void 0;
    _this.step = void 0;
    _this.scale = void 0;
    _this.className = void 0;
    _this.cw = void 0;
    _this.ch = void 0;
    _this.cx = void 0;
    _this.cy = void 0;
    _this.lscale = void 0;
    _this.lWidth = void 0;
    _this.lHeight = void 0;
    _this.left = void 0;
    _this.right = void 0;
    _this.top = void 0;
    _this.bottom = void 0;
    _this.hVisible = void 0;
    _this.vVisible = void 0;
    _this.visible = void 0;
    _this.x = _this.$public(Number, 0);
    _this.y = _this.$public(Number, 0);
    _this.tx = _this.$public(Number, 0);
    _this.ty = _this.$public(Number, 0);
    _this.l1x = _this.$public(Number, 0);
    _this.l2x = _this.$public(Number, 0);
    _this.l1y = _this.$public(Number, 0);
    _this.l2y = _this.$public(Number, 0);
    _this.step = _this.$public(Number, 0);
    _this.scale = _this.$public(Number, 0);
    _this.className = _this.$public(String, null);
    _this.cw = _this.$public(Number, 0);
    _this.ch = _this.$public(Number, 0);
    return _this;
  }

  _createClass(BaseBrick, [{
    key: "$created",
    value: function $created() {
      _get(_getPrototypeOf(BaseBrick.prototype), "$created", this).call(this);

      this.cx = this.$bind(function (x, l1x, l2x, step) {
        return step < 2 || step > 4 ? x * step : step === 2 ? 2 * (x - l1x) : 4 * (x - l2x);
      }, this.x, this.l1x, this.l2x, this.step);
      this.cy = this.$bind(function (y, l1y, l2y, step) {
        return step < 2 || step > 4 ? y * step : step === 2 ? 2 * (y - l1y) : 4 * (y - l2y);
      }, this.y, this.l1y, this.l2y, this.step);
      this.lscale = this.$bind(function (scale, step) {
        return scale / step;
      }, this.scale, this.step);
      this.lWidth = this.$bind(function (step) {
        return 40 * step;
      }, this.step);
      this.lHeight = this.$bind(function (step) {
        return 40 * step;
      }, this.step);
      this.left = this.$bind(function (x, scale, tx) {
        return x * scale + tx;
      }, this.x, this.scale, this.tx);
      this.right = this.$bind(function (left, w, scale) {
        return left + w * scale;
      }, this.left, this.lWidth, this.lscale);
      this.top = this.$bind(function (y, scale, ty) {
        return y * scale + ty;
      }, this.y, this.scale, this.ty);
      this.bottom = this.$bind(function (top, h, scale) {
        return top + h * scale;
      }, this.top, this.lHeight, this.lscale);
      this.hVisible = this.$bind(function (left, right, cw) {
        return left < cw && right > 0;
      }, this.left, this.right, this.cw);
      this.vVisible = this.$bind(function (top, bottom, ch) {
        return top < ch && bottom > 0;
      }, this.top, this.bottom, this.ch);
      this.visible = this.$bind(function (h, v, step) {
        return h && v && step > 4 || step <= 4;
      }, this.hVisible, this.vVisible, this.step);
    }
  }, {
    key: "$createDom",
    value: function $createDom() {
      var _this2 = this;

      _get(_getPrototypeOf(BaseBrick.prototype), "$createDom", this).call(this);

      this.$defTag("div", function (div) {
        div.$addClass("block");
        div.$bindClass(null, _this2.className);
        div.$defStyles({
          "transform": div.$bind(function (x, left, y, top, scale, step, v) {
            return !v ? "" : step <= 4 ? "translate(" + x + "px, " + y + "px)" : "translate(" + left + "px, " + top + "px) scale(" + scale + ")";
          }, _this2.cx, _this2.left, _this2.cy, _this2.top, _this2.lscale, _this2.step, _this2.visible),
          "will-change": div.$bind(function (step) {
            return step > 4 ? "transform" : "";
          }, _this2.step),
          "display": div.$bind(function (v) {
            return v ? '' : 'none';
          }, _this2.visible)
        });
      });
    }
  }]);

  return BaseBrick;
}(vasille_js__WEBPACK_IMPORTED_MODULE_0__.ExtensionNode);

/***/ }),

/***/ "./src/components/BetonBrick.js":
/*!**************************************!*\
  !*** ./src/components/BetonBrick.js ***!
  \**************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BetonBrick": function() { return /* binding */ BetonBrick; }
/* harmony export */ });
/* harmony import */ var _BaseBrick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BaseBrick */ "./src/components/BaseBrick.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }


var BetonBrick = /*#__PURE__*/function (_BaseBrick) {
  _inherits(BetonBrick, _BaseBrick);

  var _super = _createSuper(BetonBrick);

  function BetonBrick() {
    _classCallCheck(this, BetonBrick);

    return _super.call(this);
  }

  _createClass(BetonBrick, [{
    key: "$createDom",
    value: function $createDom() {
      _get(_getPrototypeOf(BetonBrick.prototype), "$createDom", this).call(this);

      this.className.$ = "beton-brick";
    }
  }]);

  return BetonBrick;
}(_BaseBrick__WEBPACK_IMPORTED_MODULE_0__.BaseBrick);

/***/ }),

/***/ "./src/components/BlockBrick.js":
/*!**************************************!*\
  !*** ./src/components/BlockBrick.js ***!
  \**************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BlockBrick": function() { return /* binding */ BlockBrick; }
/* harmony export */ });
/* harmony import */ var _BaseBrick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BaseBrick */ "./src/components/BaseBrick.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }


var BlockBrick = /*#__PURE__*/function (_BaseBrick) {
  _inherits(BlockBrick, _BaseBrick);

  var _super = _createSuper(BlockBrick);

  function BlockBrick() {
    _classCallCheck(this, BlockBrick);

    return _super.call(this);
  }

  _createClass(BlockBrick, [{
    key: "$created",
    value: function $created() {
      _get(_getPrototypeOf(BlockBrick.prototype), "$created", this).call(this);

      this.className.$ = "block-brick";
    }
  }]);

  return BlockBrick;
}(_BaseBrick__WEBPACK_IMPORTED_MODULE_0__.BaseBrick);

/***/ }),

/***/ "./src/components/Map.js":
/*!*******************************!*\
  !*** ./src/components/Map.js ***!
  \*******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MapV1": function() { return /* binding */ MapV1; }
/* harmony export */ });
/* harmony import */ var _types_block__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../types/block */ "./src/types/block.js");
/* harmony import */ var vasille_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vasille-js */ "./node_modules/vasille-js/src/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }



var MapV1 = /*#__PURE__*/function (_UserNode) {
  _inherits(MapV1, _UserNode);

  var _super = _createSuper(MapV1);

  function MapV1() {
    var _this;

    _classCallCheck(this, MapV1);

    _this = _super.call(this);
    _this.x = void 0;
    _this.y = void 0;
    _this.width = void 0;
    _this.height = void 0;
    _this.els = void 0;
    _this.sub = void 0;
    _this.tx = void 0;
    _this.ty = void 0;
    _this.scale = void 0;
    _this.cw = void 0;
    _this.ch = void 0;
    _this.stepScale = void 0;
    _this.x = _this.$public(Number, 0);
    _this.y = _this.$public(Number, 0);
    _this.width = _this.$public(Number, 0);
    _this.height = _this.$public(Number, 0);
    _this.els = _this.$public(vasille_js__WEBPACK_IMPORTED_MODULE_1__.SetModel);
    _this.sub = _this.$public(vasille_js__WEBPACK_IMPORTED_MODULE_1__.SetModel);
    _this.tx = _this.$public(Number, 0);
    _this.ty = _this.$public(Number, 0);
    _this.scale = _this.$public(Number, 1);
    _this.cw = _this.$public(Number, 0);
    _this.ch = _this.$public(Number, 0);
    _this.stepScale = _this.$public(Number, 1);
    return _this;
  }

  return MapV1;
}(vasille_js__WEBPACK_IMPORTED_MODULE_1__.UserNode);

/***/ }),

/***/ "./src/components/x1/MapX1.js":
/*!************************************!*\
  !*** ./src/components/x1/MapX1.js ***!
  \************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MapX1": function() { return /* binding */ MapX1; }
/* harmony export */ });
/* harmony import */ var _types_block__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../types/block */ "./src/types/block.js");
/* harmony import */ var vasille_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vasille-js */ "./node_modules/vasille-js/src/index.js");
/* harmony import */ var _Map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Map */ "./src/components/Map.js");
/* harmony import */ var _MapX2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./MapX2 */ "./src/components/x1/MapX2.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }





var MapX1 = /*#__PURE__*/function (_MapV) {
  _inherits(MapX1, _MapV);

  var _super = _createSuper(MapX1);

  function MapX1() {
    var _this;

    _classCallCheck(this, MapX1);

    _this = _super.call(this);
    _this.rtx = void 0;
    _this.rty = void 0;
    _this.rscale = void 0;
    _this.btx = void 0;
    _this.bty = void 0;
    _this.bscale = void 0;
    _this.beginTime = void 0;
    _this.endTime = void 0;
    _this.isAnim = void 0;
    _this.stepScale = _this.$bind(function (scale) {
      if (scale <= 1) {
        return 1;
      }

      if (scale <= 2) {
        return 2;
      }

      if (scale <= 4) {
        return 4;
      }

      if (scale <= 8) {
        return 8;
      }

      if (scale <= 16) {
        return 16;
      }

      if (scale <= 32) {
        return 32;
      }

      return scale;
    }, _this.scale);
    return _this;
  }

  _createClass(MapX1, [{
    key: "$created",
    value: function $created() {
      var _this2 = this;

      _get(_getPrototypeOf(MapX1.prototype), "$created", this).call(this);

      document.onmousedown = function (press) {
        var tx = _this2.tx.$;
        var ty = _this2.ty.$;
        press.preventDefault();

        document.onmousemove = function (move) {
          _this2.tx.$ = _this2.rtx = tx + (move.screenX - press.screenX);
          _this2.ty.$ = _this2.rty = ty + (move.screenY - press.screenY);
          move.preventDefault();
        };

        document.onmouseup = function (up) {
          document.onmousemove = null;
          document.onmouseup = null;
          up.preventDefault();
        };
      };

      document.onwheel = function (wheel) {
        var step = _this2.stepScale.$;
        var cscale = _this2.rscale || _this2.scale.$;
        var ctx = _this2.rtx || _this2.tx.$;
        var cty = _this2.rty || _this2.ty.$;
        var nscale;

        if (wheel.deltaY < 0) {
          var v = cscale + 0.1 * step;

          if (v < 32) {
            nscale = v;
          } else {
            nscale = 32;
          }
        } else {
          var _v = cscale - 0.1 * step;

          if (_v > 0.5) {
            nscale = _v;
          } else {
            nscale = 0.5;
          }
        }

        _this2.animTo(ctx - (nscale - cscale) * (wheel.clientX - ctx) / cscale, cty - (nscale - cscale) * (wheel.clientY - cty) / cscale, nscale);
      };

      window.onkeypress = function () {
        document.onwheel({
          deltaY: -1,
          clientX: 0,
          clientY: 0
        });
      };

      window.onresize = function () {
        _this2.cw.$ = window.innerWidth;
        _this2.ch.$ = window.innerHeight;
      };

      window.onresize();
    }
  }, {
    key: "animTo",
    value: function animTo(x, y, scale) {
      var _this3 = this;

      this.rtx = x;
      this.rty = y;
      this.rscale = scale;
      this.btx = this.tx.$;
      this.bty = this.ty.$;
      this.bscale = this.scale.$;
      this.beginTime = new Date().getTime();
      this.endTime = this.beginTime + 300;

      if (!this.isAnim) {
        this.isAnim = true;

        var update = function update() {
          var now = new Date().getTime();

          if (now > _this3.endTime) {
            _this3.isAnim = false;
            _this3.tx.$ = _this3.rtx;
            _this3.ty.$ = _this3.rty;
            _this3.scale.$ = _this3.rscale;
          } else {
            var progress = (now - _this3.beginTime) / 300;
            var step = Math.cos(Math.PI * progress) - 1;
            _this3.tx.$ = -(_this3.rtx - _this3.btx) / 2 * step + _this3.btx;
            _this3.ty.$ = -(_this3.rty - _this3.bty) / 2 * step + _this3.bty;
            _this3.scale.$ = -(_this3.rscale - _this3.bscale) / 2 * step + _this3.bscale;
            window.requestAnimationFrame(update);
          }
        };

        window.requestAnimationFrame(update);
      }
    }
  }, {
    key: "$createDom",
    value: function $createDom() {
      var _this4 = this;

      _get(_getPrototypeOf(MapX1.prototype), "$createDom", this).call(this);

      this.$defTag("div", function (div) {
        div.$addClass("map");
        div.$bindClass(null, _this4.$bind(function (stepScale) {
          return "x" + stepScale;
        }, _this4.stepScale));
        div.$defStyles({
          "width": div.$bind(function (w, s) {
            return (s === 2 ? w : 0) + "px";
          }, _this4.width, _this4.stepScale),
          "height": div.$bind(function (h, s) {
            return (s === 2 ? h : 0) + "px";
          }, _this4.height, _this4.stepScale),
          "transform": div.$bind(function (x, tx, y, ty, scale, stepScale) {
            if (stepScale === 1) {
              return "translate(" + (x + tx) + "px, " + (y + ty) + "px) scale(" + scale + ")";
            } else {
              return "";
            }
          }, _this4.x, _this4.tx, _this4.y, _this4.ty, _this4.scale, _this4.stepScale),
          "position": div.$bind(function (step) {
            return step === 1 ? "absolute" : "static";
          }, _this4.stepScale),
          "will-change": div.$bind(function (step) {
            return step === 1 ? "transform" : "";
          }, _this4.stepScale)
        });
        div.$defRepeater(new vasille_js__WEBPACK_IMPORTED_MODULE_1__.SetView(), function ($) {
          $.model = _this4.sub;
        }, function (node, item) {
          node.$defElement(new _MapX2__WEBPACK_IMPORTED_MODULE_3__.MapX2(), function ($) {
            $.x = item.x;
            $.y = item.y;
            $.width = item.width;
            $.height = item.height;
            $.els = item.els;
            $.sub = item.sub;
            $.tx = _this4.tx;
            $.ty = _this4.ty;
            $.scale = _this4.scale;
            $.cw = _this4.cw;
            $.ch = _this4.ch;
            $.stepScale = _this4.stepScale;
          });
        });
      });
    }
  }]);

  return MapX1;
}(_Map__WEBPACK_IMPORTED_MODULE_2__.MapV1);

/***/ }),

/***/ "./src/components/x1/MapX2.js":
/*!************************************!*\
  !*** ./src/components/x1/MapX2.js ***!
  \************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MapX2": function() { return /* binding */ MapX2; }
/* harmony export */ });
/* harmony import */ var _types_block__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../types/block */ "./src/types/block.js");
/* harmony import */ var vasille_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vasille-js */ "./node_modules/vasille-js/src/index.js");
/* harmony import */ var _Map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Map */ "./src/components/Map.js");
/* harmony import */ var _MapX4__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./MapX4 */ "./src/components/x1/MapX4.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }





var MapX2 = /*#__PURE__*/function (_MapV) {
  _inherits(MapX2, _MapV);

  var _super = _createSuper(MapX2);

  function MapX2() {
    var _this;

    _classCallCheck(this, MapX2);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    _this.lWidth = void 0;
    _this.lHeight = void 0;
    _this.lScale = void 0;
    _this.left = void 0;
    _this.right = void 0;
    _this.top = void 0;
    _this.bottom = void 0;
    _this.hVisible = void 0;
    _this.vVisible = void 0;
    _this.visible = void 0;
    return _this;
  }

  _createClass(MapX2, [{
    key: "$created",
    value: function $created() {
      _get(_getPrototypeOf(MapX2.prototype), "$created", this).call(this);

      this.lWidth = this.$bind(function (w, step) {
        return w * step;
      }, this.width, this.stepScale);
      this.lHeight = this.$bind(function (h, step) {
        return h * step;
      }, this.height, this.stepScale);
      this.lScale = this.$bind(function (scale, step) {
        return scale / step;
      }, this.scale, this.stepScale);
      this.left = this.$bind(function (x, scale, tx) {
        return x * scale + tx;
      }, this.x, this.scale, this.tx);
      this.right = this.$bind(function (left, w, scale) {
        return left + w * scale;
      }, this.left, this.lWidth, this.lScale);
      this.top = this.$bind(function (y, scale, ty) {
        return y * scale + ty;
      }, this.y, this.scale, this.ty);
      this.bottom = this.$bind(function (top, h, scale) {
        return top + h * scale;
      }, this.top, this.lHeight, this.lScale);
      this.hVisible = this.$bind(function (left, right, cw) {
        return left < cw && right > 0;
      }, this.left, this.right, this.cw);
      this.vVisible = this.$bind(function (top, bottom, ch) {
        return top < ch && bottom > 0;
      }, this.top, this.bottom, this.ch);
      this.visible = this.$bind(function (h, v) {
        return h && v;
      }, this.hVisible, this.vVisible);
    }
  }, {
    key: "$createDom",
    value: function $createDom() {
      var _this2 = this;

      _get(_getPrototypeOf(MapX2.prototype), "$createDom", this).call(this);

      this.$defTag("div", function (div) {
        div.$addClasses("map", "l1");
        div.$defStyles({
          "width": div.$bind(function (w, s) {
            return (s === 2 ? w : 0) + "px";
          }, _this2.lWidth, _this2.stepScale),
          "height": div.$bind(function (h, s) {
            return (s === 2 ? h : 0) + "px";
          }, _this2.lHeight, _this2.stepScale),
          "transform": div.$bind(function (left, top, scale, stepScale, v) {
            if (stepScale === 2 && v) {
              return "translate(" + left + "px, " + top + "px) scale(" + scale + ")";
            } else {
              return "";
            }
          }, _this2.left, _this2.top, _this2.lScale, _this2.stepScale, _this2.visible),
          "position": div.$bind(function (step) {
            return step === 2 ? "absolute" : "static";
          }, _this2.stepScale),
          "will-change": div.$bind(function (v, step) {
            return step === 2 && v ? "transform" : "";
          }, _this2.visible, _this2.stepScale),
          "display": div.$bind(function (v, step) {
            return v || step !== 2 ? '' : 'none';
          }, _this2.visible, _this2.stepScale)
        });
        div.$defRepeater(new vasille_js__WEBPACK_IMPORTED_MODULE_1__.SetView(), function ($) {
          $.model = _this2.sub;
        }, function (node, item) {
          node.$defElement(new _MapX4__WEBPACK_IMPORTED_MODULE_3__.MapX4(), function ($) {
            $.x = $.$bind(function (x) {
              return x;
            }, item.x);
            $.y = $.$bind(function (y) {
              return y;
            }, item.y);
            $.width = $.$bind(function (w) {
              return w;
            }, item.width);
            $.height = $.$bind(function (h) {
              return h;
            }, item.height);
            $.els = item.els;
            $.sub = item.sub;
            $.tx = $.$bind(function (tx) {
              return tx;
            }, _this2.tx);
            $.ty = $.$bind(function (ty) {
              return ty;
            }, _this2.ty);
            $.scale = $.$bind(function (s) {
              return s;
            }, _this2.scale);
            $.cw = _this2.cw;
            $.ch = _this2.ch;
            $.stepScale = $.$bind(function (s) {
              return s;
            }, _this2.stepScale);
            $.l1x = _this2.x;
            $.l1y = _this2.y;
          }, function (x4) {
            x4.$bindShow(_this2.visible);
          });
        });
      });
    }
  }]);

  return MapX2;
}(_Map__WEBPACK_IMPORTED_MODULE_2__.MapV1);

/***/ }),

/***/ "./src/components/x1/MapX4.js":
/*!************************************!*\
  !*** ./src/components/x1/MapX4.js ***!
  \************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MapX4": function() { return /* binding */ MapX4; }
/* harmony export */ });
/* harmony import */ var _types_block__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../types/block */ "./src/types/block.js");
/* harmony import */ var vasille_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vasille-js */ "./node_modules/vasille-js/src/index.js");
/* harmony import */ var _BetonBrick__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../BetonBrick */ "./src/components/BetonBrick.js");
/* harmony import */ var _BlockBrick__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../BlockBrick */ "./src/components/BlockBrick.js");
/* harmony import */ var _Map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Map */ "./src/components/Map.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }






var MapX4 = /*#__PURE__*/function (_MapV) {
  _inherits(MapX4, _MapV);

  var _super = _createSuper(MapX4);

  function MapX4(props) {
    var _this;

    _classCallCheck(this, MapX4);

    _this = _super.call(this, props);
    _this.l1x = void 0;
    _this.l1y = void 0;
    _this.lWidth = void 0;
    _this.lHeight = void 0;
    _this.lScale = void 0;
    _this.left = void 0;
    _this.right = void 0;
    _this.top = void 0;
    _this.bottom = void 0;
    _this.hVisible = void 0;
    _this.vVisible = void 0;
    _this.visible = void 0;
    _this.l1x = _this.$public(Number, 0);
    _this.l1y = _this.$public(Number, 0);
    return _this;
  }

  _createClass(MapX4, [{
    key: "$created",
    value: function $created() {
      _get(_getPrototypeOf(MapX4.prototype), "$created", this).call(this);

      this.lWidth = this.$bind(function (w, step) {
        return w * step;
      }, this.width, this.stepScale);
      this.lHeight = this.$bind(function (h, step) {
        return h * step;
      }, this.height, this.stepScale);
      this.lScale = this.$bind(function (scale, step) {
        return scale / step;
      }, this.scale, this.stepScale);
      this.left = this.$bind(function (x, scale, tx) {
        return x * scale + tx;
      }, this.x, this.scale, this.tx);
      this.right = this.$bind(function (left, w, scale) {
        return left + w * scale;
      }, this.left, this.lWidth, this.lScale);
      this.top = this.$bind(function (y, scale, ty) {
        return y * scale + ty;
      }, this.y, this.scale, this.ty);
      this.bottom = this.$bind(function (top, h, scale) {
        return top + h * scale;
      }, this.top, this.lHeight, this.lScale);
      this.hVisible = this.$bind(function (left, right, cw) {
        return left < cw && right > 0;
      }, this.left, this.right, this.cw);
      this.vVisible = this.$bind(function (top, bottom, ch) {
        return top < ch && bottom > 0;
      }, this.top, this.bottom, this.ch);
      this.visible = this.$bind(function (h, v) {
        return h && v;
      }, this.hVisible, this.vVisible);
    }
  }, {
    key: "$createDom",
    value: function $createDom() {
      var _this2 = this;

      _get(_getPrototypeOf(MapX4.prototype), "$createDom", this).call(this);

      this.$defTag("div", function (div) {
        div.$addClasses("map", "l2", 'i' + ++MapX4.i);
        div.$defStyles({
          "width": div.$bind(function (w, s) {
            return (s === 4 ? w : 0) + "px";
          }, _this2.lWidth, _this2.stepScale),
          "height": div.$bind(function (h, s) {
            return (s === 4 ? h : 0) + "px";
          }, _this2.lHeight, _this2.stepScale),
          "transform": div.$bind(function (top, left, scale, stepScale, v) {
            if (stepScale === 4 && v) {
              return "translate(" + left + "px, " + top + "px) scale(" + scale + ")";
            } else {
              return "";
            }
          }, _this2.top, _this2.left, _this2.lScale, _this2.stepScale, _this2.visible),
          "position": div.$bind(function (step) {
            return step === 4 ? "absolute" : "static";
          }, _this2.stepScale),
          "will-change": div.$bind(function (v, step) {
            return step === 4 && v ? "transform" : "";
          }, _this2.visible, _this2.stepScale),
          "display": div.$bind(function (v, step) {
            return v || step !== 4 ? 'block' : 'none';
          }, _this2.visible, _this2.stepScale)
        });

        var props = function props($, item) {
          $.x = item.x;
          $.y = item.y;
          $.tx = $.$bind(function (tx) {
            return tx;
          }, _this2.tx);
          $.ty = $.$bind(function (ty) {
            return ty;
          }, _this2.ty);
          $.l1x = _this2.l1x;
          $.l1y = _this2.l1y;
          $.l2x = _this2.x;
          $.l2y = _this2.y;
          $.step = $.$bind(function (s) {
            return s;
          }, _this2.stepScale);
          $.scale = $.$bind(function (s) {
            return s;
          }, _this2.scale);
          $.cw = _this2.cw;
          $.ch = _this2.ch;
        };

        div.$defRepeater(new vasille_js__WEBPACK_IMPORTED_MODULE_1__.SetView(), function ($) {
          $.model = _this2.els;
        }, function (node, item) {
          if (item instanceof _types_block__WEBPACK_IMPORTED_MODULE_0__.BrickBlock) {
            node.$defElement(new _BlockBrick__WEBPACK_IMPORTED_MODULE_3__.BlockBrick(), function ($) {
              return props($, item);
            }, function (brick) {// brick.$bindShow(this.visible);
            });
          }

          if (item instanceof _types_block__WEBPACK_IMPORTED_MODULE_0__.BetonBlock) {
            node.$defElement(new _BetonBrick__WEBPACK_IMPORTED_MODULE_2__.BetonBrick(), function ($) {
              return props($, item);
            }, function (brick) {// brick.$bindShow(this.visible);
            });
          }
        });
      });
    }
  }]);

  return MapX4;
}(_Map__WEBPACK_IMPORTED_MODULE_4__.MapV1);
MapX4.i = 1;

/***/ }),

/***/ "./src/types/block.js":
/*!****************************!*\
  !*** ./src/types/block.js ***!
  \****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Block": function() { return /* binding */ Block; },
/* harmony export */   "BrickBlock": function() { return /* binding */ BrickBlock; },
/* harmony export */   "BetonBlock": function() { return /* binding */ BetonBlock; },
/* harmony export */   "BlockPack": function() { return /* binding */ BlockPack; }
/* harmony export */ });
/* harmony import */ var vasille_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vasille-js */ "./node_modules/vasille-js/src/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }


var Block = function Block(x, y) {
  _classCallCheck(this, Block);

  this.x = void 0;
  this.y = void 0;
  this.x = new vasille_js__WEBPACK_IMPORTED_MODULE_0__.Reference(x);
  this.y = new vasille_js__WEBPACK_IMPORTED_MODULE_0__.Reference(y);
};
var BrickBlock = /*#__PURE__*/function (_Block) {
  _inherits(BrickBlock, _Block);

  var _super = _createSuper(BrickBlock);

  function BrickBlock(x, y) {
    _classCallCheck(this, BrickBlock);

    return _super.call(this, x, y);
  }

  return BrickBlock;
}(Block);
var BetonBlock = /*#__PURE__*/function (_Block2) {
  _inherits(BetonBlock, _Block2);

  var _super2 = _createSuper(BetonBlock);

  function BetonBlock(x, y) {
    _classCallCheck(this, BetonBlock);

    return _super2.call(this, x, y);
  }

  return BetonBlock;
}(Block);
var BlockPack = function BlockPack(x, y, width, height) {
  _classCallCheck(this, BlockPack);

  this.x = void 0;
  this.y = void 0;
  this.width = void 0;
  this.height = void 0;
  this.els = void 0;
  this.sub = void 0;
  this.x = new vasille_js__WEBPACK_IMPORTED_MODULE_0__.Reference(x);
  this.y = new vasille_js__WEBPACK_IMPORTED_MODULE_0__.Reference(y);
  this.width = new vasille_js__WEBPACK_IMPORTED_MODULE_0__.Reference(width);
  this.height = new vasille_js__WEBPACK_IMPORTED_MODULE_0__.Reference(height);
  this.els = new vasille_js__WEBPACK_IMPORTED_MODULE_0__.SetModel();
  this.sub = new vasille_js__WEBPACK_IMPORTED_MODULE_0__.SetModel();
};

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
!function() {
/*!*********************!*\
  !*** ./src/main.js ***!
  \*********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.js */ "./src/App.js");

var app = new _App_js__WEBPACK_IMPORTED_MODULE_0__.App();
app.createLevel([[2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 2, 2, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 2, 2, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0], [2, 2, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 2, 2], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]);
app.$init({});
window.app = app;
}();
/******/ })()
;
//# sourceMappingURL=app.js.map